<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('entity_id')->index()->nullable();
            $table->foreign('entity_id')->references('id')->on('entities');

            $table->string("asseteable_type")->nullable();
            $table->unsignedInteger("asseteable_id")->nullable();
            $table->index(["asseteable_type", "asseteable_id"], 'asseteable');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
