<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMultiAssetImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multi_asset_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');

            $table->string("imageable_type")->nullable();
            $table->unsignedInteger("imageable_id")->nullable();
            $table->index(["imageable_type", "imageable_id"], 'imageable');
        
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multi_asset_images');
    }
}
