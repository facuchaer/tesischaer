<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateApartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('apartment_assets', function (Blueprint $table) {
            $table->integer('typology_id')->unsigned()->index()->nullable();
            $table->foreign('typology_id')->references('id')->on('typologies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('apartment_assets', function (Blueprint $table) {
            $table->dropForeign('apartment_assets_typology_id_foreign');
            $table->dropColumn(['typology_id']);
        });
    }
}
