<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApartmentAssetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apartment_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('description')->default('');
            $table->float('price');
            $table->float('surface');
            $table->integer('rooms_amount');
            $table->integer('status');
            $table->integer('typology');
            $table->integer('orientation');
            $table->text('gen_field_1');
            $table->text('gen_field_2');
            $table->text('gen_field_3');

            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apartment_assets');
    }
}
