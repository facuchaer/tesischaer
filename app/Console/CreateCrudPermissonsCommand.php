<?php

namespace App\Console;

use Illuminate\Console\Command;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateCrudPermissonsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'superbackend:permissions:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create all CRUD permissions for current models.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $app = app_path();
        $classPaths = glob($app . '/Models/*.php');
        array_merge($classPaths, glob($app . '/Models/**/*.php'));
        $classes = array();
        foreach ($classPaths as $classPath) {
            $segments = explode('/', $classPath);
            $segments = explode('\\', $segments[count($segments) - 1]);
            $classes[] = basename($segments[count($segments) - 1], '.php');
        }
        
        $classes[] = 'User';
        $classes[] = 'Role';
        $classes[] = 'Permission';

        $edit_admin_tables = ['Role','Permission','User','Company','Project','Agent','Group'];
        $view_admin_tables = ['Role','Permission','User','Company','Agent'];

        if(count($classes) < 1){
            $this->error('No Classes found on '.$app.' !');
            return;
        }

        $super_editor = Role::where('name', 'like', 'Super Editor')->first();
        $editor = Role::where('name', 'like', 'Editor')->first();
        

        foreach ($classes as $class) {

            app()['cache']->forget('spatie.permission.cache');

            $perm = Permission::findOrCreate('edit own ' . $class . ' content');
            if(!in_array($class,$edit_admin_tables) ){
                $super_editor->givePermissionTo($perm);
                $editor->givePermissionTo($perm);
            }
            Permission::findOrCreate('edit any ' . $class . ' content');
            Permission::findOrCreate('delete own ' . $class . ' content');
            Permission::findOrCreate('delete any ' . $class . ' content');
            Permission::findOrCreate('create ' . $class . ' content');
            $perm =  Permission::findOrCreate('view own ' . $class . ' content');
            if(!in_array($class,$view_admin_tables) ){
                $super_editor->givePermissionTo($perm);
                $editor->givePermissionTo($perm);
            }
            Permission::findOrCreate('view any ' . $class . ' content');

            $this->info('Crud Permissions created for Model '. $class .'.');

        }

        Permission::findOrCreate('manage users');
        $perm = Permission::findOrCreate('manage assets');
        $super_editor->givePermissionTo($perm);
        $editor->givePermissionTo($perm);
        Permission::findOrCreate('view admin tabs');

        // $super_admin = Role::where('name', 'like', 'Super Admin')->first();
        // $super_admin->syncPermissions(Permission::all());
        $super_admin = Role::where('name', 'like', 'Admin')->first();
        $super_admin->syncPermissions(Permission::all());
    }
}
