<?php

namespace App\Console;

use Illuminate\Support\Str;
use Illuminate\Console\Command;

class ApiCreateCommand extends Command
{
    /**
     * The name of the console command.
     *
     * @var string
     */
    protected $signature = 'superbackend:make:api 
            {--a|all : make both resource, collection and controllers} 
            {--r|resource : make only resource} 
            {--col|collection : make only collection} 
            {--con|controllers : make all controllers for api} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create all api resources classes for current project.';

    /**
     * Flags sorry 
     */
    private $collection = false;
    private $resource = false;
    private $controller = false;


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $app = app_path();
        $classPaths = glob($app . '/Models/*.php');
        array_merge($classPaths, glob($app . '/Models/**/*.php'));
        $classes = array();
        foreach ($classPaths as $classPath) {
            $segments = explode('/', $classPath);
            $segments = explode('\\', $segments[count($segments) - 1]);
            $classes[] = basename($segments[count($segments) - 1], '.php');
        }

        if(count($classes) < 1){
            $this->error('No Classes found on '.$app.' !');
            return;
        }
        
        
        foreach ($classes as $class) {
            
            if ($this->option('all')) {
                $this->collection = true;
                $this->resource = true;
                $this->controller = true;
            }

            if( $this->resource || $this->option('resource')){ 
                $this->createResource($class);
                $this->info('Api resource created for Model '. $class .'.');
            }

            if($this->collection || $this->option('collection')){
                $this->createCollection($class);
                $this->info('Api collection created for Model '. $class .'.');
            }

            if($this->controller || $this->option('controllers')){
                $this->createController($class);
                $this->info('Api controller created for Model '. $class .'.');
            }

        }
    }

    protected function createCollection($class){

        $resource = Str::studly(class_basename($class));
        $this->call('superbackend:make:api:resource', [
            'name' => "{$resource}Collection",
            '--collection' => true
        ]);
    }

    protected function createResource($class){
        $resource = Str::studly(class_basename($class));
        $this->call('superbackend:make:api:resource', [
            'name' => "{$resource}"
        ]);
    }

    protected function createController($class){
        $resource = Str::studly(class_basename($class));
        $this->call('superbackend:make:controller', [
            'name' => "{$resource}",
            '--api' => true,
            '--model' => "App/Models/{$resource}"
        ]);
    }
}
