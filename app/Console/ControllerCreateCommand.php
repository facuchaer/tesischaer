<?php

namespace App\Console;
use Yaml;
use Illuminate\Routing\Console\ControllerMakeCommand;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputOption;

class ControllerCreateCommand extends ControllerMakeCommand
{
    protected $yamlContents;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'superbackend:make:controller';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new controller class for model/s {api} / {admin}';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Controller';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {

        $stub = '/stubs/controller.model.stub';

        if ($this->option('api')) {
            $stub = str_replace('.stub', '.api.stub', $stub);
        }
        return __DIR__.$stub;
    }

    public function handle(){

        if(!$this->option('model')){
            $this->error('Must specify a model --model="App/Models/{modelname}"');
            return false;
        }

        if(!file_exists(app_path().'/Models/Models.yaml')){
            $this->error('Missing '.app_path().'/Models/Models.yaml file.');
            return false;
        }

        $this->yamlContents = Yaml::parse(file_get_contents(app_path().'/Models/Models.yaml'));

        if(!is_array($this->yamlContents) || !array_key_exists($this->getNameInput(),$this->yamlContents) || $this->yamlContents[$this->getNameInput()] === NULL){
            $this->error('Resource '.$this->getNameInput().' not found, looken into: '.app_path().'/Models/Models.yaml.');
            return false;
        }

        $this->yamlContents = $this->yamlContents[$this->getNameInput()];

        if($this->option('api')){
            $name = $this->qualifyClass('Api/'.Str::plural($this->getNameInput()).'Controller');
        }elseif($this->option('admin')){
            $this->info('Sorry Resource not supported yet, will create it empty.');
            $name = $this->qualifyClass('Admin/'.Str::plural($this->getNameInput()).'Controller');
        }else{
            $this->error('No Controller type selected use --admin or --api.');
            return false;
        }

        $path = $this->getPath($name);

        

        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
        if ((! $this->hasOption('force') ||
             ! $this->option('force')) &&
             $this->alreadyExists($name)) 
        {
            $this->error($this->type." {$name} already exists!");

            return false;
        }

        

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $this->files->put($path, $this->buildClass($name));

        $this->info($this->type." {$name} created successfully.");
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        if ($this->option('model')) {
            $stub = $this->modelReplacements($stub);
        }

        return $this->replaceNamespace($stub, $name)->replaceSuperbackend($stub,$this->yamlContents)->replaceClass($stub, $name);
    }

       /**
     * Build the model replacement values.
     *
     * @param  array  $replace
     * @return array
     */
    protected function modelReplacements(&$stub)
    {
        $modelClass = $this->parseModel($this->option('model'));

        if (! class_exists($modelClass)) {
            if ($this->confirm("A {$modelClass} model does not exist. Do you want to generate it?", true)) {
                $this->call('make:model', ['name' => $modelClass]);
            }else{
                $this->error('Must create or specify a model. --model="App/Models/{modelname}"');
                exit();
            }
        }

        $stub = str_replace(
            ['DummyFullModelClass', 'DummyModelClass', 'DummyModelVariable'],
            [   
                $modelClass,
                class_basename($modelClass),
                lcfirst(class_basename($modelClass)),
            ],
            $stub
        );

        return $stub;
    }

    /**
     * Replace the superbackend stuff for the given stub.
     *
     * @param  string  $stub
     * @param  array  $yamlContents
     * @return $this
     */
    protected function replaceSuperbackend(&$stub, $yamlContents)
    {
        $this->validateYaml($yamlContents);

        $stub = str_replace(
            ['DummyTitle', 'DummyResourceAlias','DummyRouteAlias','DummyPermissionName'],
            [   $yamlContents['title'], 
                $yamlContents['resource_alias'],
                $yamlContents['route_alias'],
                $this->getNameInput()
            ],
            $stub
        );

        $stub = str_replace(
            ['DummyApiRelations'],
            [   
                array_key_exists('api_resources', $yamlContents) ? $this->getApiRelations($yamlContents['api_resources']) : '',
            ],
            $stub
        );
        
        $stub = str_replace(
            ['DummyRelations'],
            [   
                array_key_exists('relations', $yamlContents) ? implode("','" ,$yamlContents['relations']) : '', 
            ],
            $stub
        );

        if(array_key_exists('validation', $yamlContents)){
            $stub = str_replace(
                ['DummyStoreValidation','DummyUpdateValidation','DummyDeleteValidation'],
                [   
                    array_key_exists('store',$yamlContents['validation']) ? $this->toStringValidation($yamlContents['validation']['store']) : '' ,
                    array_key_exists('update',$yamlContents['validation']) ? $this->toStringValidation($yamlContents['validation']['update']) : '', 
                    array_key_exists('delete',$yamlContents['validation']) ? $this->toStringValidation($yamlContents['validation']['delete']) : '', 
                ],
                $stub
            );
        }

        return $this;
    }

    private function toStringValidation($validation, string $concat=''){
        foreach ($validation as $key => $resource) {
            if(is_array($resource)){
                $concat.= "'".$key."'"."=>[".$this->toStringValidation($resource, $concat)."],\n\t\t";
                
            }else{
                $concat.= "'".$key."'"."=>'".$resource."',\n\t\t\t";
            }
        }
        return $concat;
    }

    private function getApiRelations($relations, string $concat=''){
        foreach ($relations as $key => $resource) {
            if(is_array($resource)){
                $concat.= "'".$key."'"."=>[".$this->getApiRelations($resource, $concat)."],\n\t\t";
                
            }else{
                $concat.= "'".$key."'"."=>'".$resource."',\n\t\t";
            }
        }
        return $concat;
    }

    private function validateYaml($yamlContents){
        $exit = false;
        $missing = '** ';
        if(!array_key_exists('title',$yamlContents)){
            $missing.='title ';
            $exit=true;
        }
        if(!array_key_exists('resource_alias',$yamlContents)){
            $missing.='resource_alias ';
            $exit=true;
        }
        if(!array_key_exists('route_alias',$yamlContents)){
            $missing.='route_alias ';
            $exit=true;
        }
        $missing.='**';

        if($exit){
            $this->error('Missing '.$missing.' for yaml resource '.$this->getNameInput());
            exit();
        }

    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['admin', 'a', InputOption::VALUE_NONE, 'Generate a admin controller class.'],
            ['model', 'm', InputOption::VALUE_OPTIONAL, 'Generate a resource controller for the given model.'],
            ['api', null, InputOption::VALUE_NONE, 'Genrate api controller class.'],
            ['force', 'f', InputOption::VALUE_NONE, 'Forces creation.'],
        ];
    }
}
