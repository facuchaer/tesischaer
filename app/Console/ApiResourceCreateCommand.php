<?php

namespace App\Console;

use Illuminate\Support\Str;
use Yaml;
use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputOption;

class ApiResourceCreateCommand extends GeneratorCommand
{
    protected $yamlContents;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'superbackend:make:api:resource';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new superbackend resource';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Resource';

    /**
     * Execute the console command.
     *
     * @return bool|null
     */
    public function handle()
    {
        $modelName = $this->getNameInput();
        if ($this->collection()) {
            $this->type = 'Resource collection';
            $modelName = str_replace('Collection','',$this->getNameInput());
        }
        
        $this->yamlContents = Yaml::parse(file_get_contents(app_path().'/Models/Models.yaml'));

        if(!is_array($this->yamlContents) || !array_key_exists($modelName,$this->yamlContents) || $this->yamlContents[$modelName] === NULL){
            $this->error('Resource '.$modelName.' not found, looken into: '.app_path().'/Models/Models.yaml.');
            return false;
        }

        $this->yamlContents = $this->yamlContents[$modelName];

        parent::handle();
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return $this->collection()
                    ? __DIR__.'/stubs/resource-collection.stub'
                    : __DIR__.'/stubs/resource.stub';
    }

    /**
     * Determine if the command is generating a resource collection.
     *
     * @return bool
     */
    protected function collection()
    {
        return $this->option('collection') ||
               Str::endsWith($this->argument('name'), 'Collection');
    }

    /**
     * Build the class with the given name.
     *
     * @param  string  $name
     * @return string
     */
    protected function buildClass($name)
    {
        $stub = $this->files->get($this->getStub());

        return $this->replaceNamespace($stub, $name)->replaceModelRelations($stub,$this->yamlContents)->replaceClass($stub, $name);
    }

    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Resources';
    }

    /**
     * Replace the namespace for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return $this
     */
    protected function replaceModelRelations(&$stub, $yaml)
    {
        $stub = str_replace(
            ['DummyResourceRelation'],
            [
                array_key_exists('api_resources', $yaml) ? $this->getApiRelations($yaml['api_resources']) : '',
            ],
            $stub
        );

        return $this;
    }

    private function getApiRelations($relations, string $concat=''){
        foreach ($relations as $key => $resource) {
            if(is_array($resource)){
                $concat.= "'".$key."'"."=>[".$this->getApiRelations($resource, $concat)."],\n\t\t";
                
            }else{
                $concat.= "'".$key."'"." => ".$resource.'::collection($this->whenLoaded('."'".$key."')),\n\t\t";
            }
        }
        
        return $concat;
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['collection', 'c', InputOption::VALUE_NONE, 'Create a resource collection.']
        ];
    }
}
