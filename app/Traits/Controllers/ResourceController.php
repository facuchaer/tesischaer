<?php

namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Company;

trait ResourceController
{
    use ResourceHelper;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $auth = $this->getAuthorization('view');

        $paginatorData = [];
        $perPage = (int) $request->input('per_page', '');
        $perPage = (is_numeric($perPage) && $perPage > 0 && $perPage <= 100) ? $perPage : 15;
        if ($perPage != 15) {
            $paginatorData['per_page'] = $perPage;
        }

        $search = trim($request->input('search', ''));
        if (! empty($search)) {
            $paginatorData['search'] = $search;
        }

        $records = $this->getSearchRecords($request, $perPage, $search, $auth);
        $records->appends($paginatorData);

        return view($this->filterIndexView('_resources.index'), $this->filterSearchViewData($request, [
            'records' => $records,
            'search' => $search,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'permissionName' => $this->getPermissionName(),
            'resourceTitle' => $this->getResourceTitle(),
            'viewResources' => $this->getResourcesForView(),
            'superFilters' => $this->getSuperFilterForView(),
            'perPage' => $perPage
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auth = $this->getAuthorization('create');

        $class = $this->getResourceModel();

        $record = new $class();

        return view($this->filterCreateView('_resources.create'), $this->filterCreateViewData([
            'record' => new $class(),
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'permissionName' => $this->getPermissionName(),
            'resourceTitle' => $this->getResourceTitle(),
            'viewResources' => $this->getResourcesForView(),
            'superFilters' => $this->getSuperFilterForView(),
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->getResourceModel());

        $valuesToSave = $this->getValuesToSave($request);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'store');

        if ($record = $this->getResourceModel()::create($this->alterValuesToSave($request, $valuesToSave))) {
            $this->afterCreateActions($request,$record);
            flash()->success('Element successfully inserted.');

            if($request->input('clone') && $request->input('clone') == 1){
                $new_record = $record->replicate();
                $new_record->name = $new_record->name . '-clone';
                $this->alterCloneData($request, $record, $new_record);
                $new_record->push();
                return $this->show($new_record->id);

            }else{
                return $this->getRedirectAfterSave($record);
            }
            
        } else {
            flash()->info('Element was not inserted.');
        }



        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect(route($this->getResourceRoutesAlias().'.edit', $id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('update', $record);
        
        return view($this->filterEditView('_resources.edit'), $this->filterEditViewData($record, [
            'record' => $record,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'permissionName' => $this->getPermissionName(),
            'resourceTitle' => $this->getResourceTitle(),
            'viewResources' => $this->getResourcesForView(),
            'superFilters' => $this->getSuperFilterForView(),
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('update', $record);

        $valuesToSave = $this->getValuesToSave($request, $record);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'update', $record);

        if ($record->update($this->alterValuesToSave($request, $valuesToSave))) {

            $this->afterEditActions($request,$record);
            
            flash()->success('Element successfully updated.');

            if($request->input('clone') && $request->input('clone') == 1){
                $new_record = $record->replicate();
                $new_record->name = $new_record->name . '-clone';
                $this->alterCloneData($request, $record, $new_record);
                $new_record->push();
                return $this->show($new_record->id);

            }else{
                return $this->getRedirectAfterSave($record);
            }
        } else {
            flash()->info('Element was not updated.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('delete', $record);

        if (! $this->checkDestroy($record)) {
            return redirect(route($this->getResourceRoutesAlias().'.index'));
        }

        if ($record->delete()) {
            flash()->success('Element successfully deleted.');
        } else {
            flash()->info('Element was not deleted.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    /**
     * Delete all selected records at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = $request->input('ids');// $this->getResourceModel()::whereIn('id', $request->input('ids'))->get();
            
            foreach ($entries as $id) {
                $entity = $this->getResourceModel()::findOrFail($id);
                $entity->delete();
            }
        }
    }

    /**
     * Clone all selected records at once.
     *
     * @param Request $request
     */
    public function massClone(Request $request)
    {
        if ($request->input('ids')) {
            $entries = $request->input('ids');
            $amount = $request->input('clone-amount');

            $relations = $this->getModelRelations();
            
            if($amount > 0){
                foreach ($entries as $id) {
                    //load entity with all relations
                    $entity = $this->getResourceModel()::findOrFail($id);
                    if($entity){

                        $entity->load($relations);                        

                        for( $i=0; $i < $amount; $i++ ){
                            
                            $new_entity = $entity->replicate();
                            $new_entity->name = $new_entity->name.'-clone';
                            $new_entity = $this->alterCloneData($request, $entity, $new_entity);
                            $new_entity->push();

                            $this->afterCloneActions($request, $entity, $new_entity);
                        }
                    }
                }
                flash()->success('Replicated '. $amount . ' '.$this->getResourceTitle());
            }else{
                flash()->error('Amount must be greater than 0');
            }
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    private function getSuperFilterForView(){

        $companies = Auth::user()->is_admin ? Company::all()->pluck('name','id')->toArray() : Auth::user()->companies->pluck('name','id')->toArray();

        // foreach ($companies as $key => $company) {
        //     //si no hay array lo creo
        //     if(!isset($companies['project_groups'])){
        //         $companies[$key]['project_groups'] = [];
        //     }
        //     //agrego todos los porject groups 
        //     $companies[$key]['project_groups'][]= ProjectGroup::whereIn('company_id', $key )->pluck('name','id')->toArray();

        //     //
        //     foreach ($companies[$company]['project_groups'] as $key => $pg) {
        //         if(!isset($companies[$company]['project_groups'][$pg]['projects'])){
        //             $companies[$company]['project_groups'][$pg]['projects'] = [];
        //         }

        //         $companies[$company]['project_groups'][$pg]['projects'][] = Project::whereIn('project_group_id', $pg )->pluck('name','id')->toArray();
        //     }
        // }




        return['companies' => $companies]; 
    }
}
