<?php

namespace App\Traits\Controllers;

use Illuminate\Http\Request;
use App\Models\Company;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;

trait ApiResourceController
{
    use ApiResourceHelper;

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $auth = $this->getAuthorization('view');

        $perPage = (int) $request->input('per_page', '');
        $perPage = (is_numeric($perPage) && $perPage > 0 && $perPage <= 100) ? $perPage : 15;
        if ($perPage != 15) {
            $paginatorData['per_page'] = $perPage;
        }
        
        $records = $this->getResourceModel()::paginate($perPage);
        $records->load($this->getModelRelations());
        return $records;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $auth = $this->getAuthorization('create');

        $class = $this->getResourceModel();

        $record = new $class();

        return view($this->filterCreateView('_resources.create'), $this->filterCreateViewData([
            'record' => new $class(),
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'permissionName' => $this->getPermissionName(),
            'resourceTitle' => $this->getResourceTitle(),
            'viewResources' => $this->getResourcesForView()
        ]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', $this->getResourceModel());

        $valuesToSave = $this->getValuesToSave($request);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'store');

        if ($record = $this->getResourceModel()::create($this->alterValuesToSave($request, $valuesToSave))) {
            $this->afterCreateActions($request,$record);
            flash()->success('Element successfully inserted.');

            if($request->input('clone') && $request->input('clone') == 1){
                $new_record = $record->replicate();
                $new_record->name = $new_record->name . '-clone';
                $this->alterCloneData($request, $record, $new_record);
                $new_record->push();
                return $this->show($new_record->id);

            }else{
                return $this->getRedirectAfterSave($record);
            }
            
        } else {
            flash()->info('Element was not inserted.');
        }



        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $records = $this->getResourceModel()::findOrFail($id);
        $records->load($this->getModelRelations());
        return $records;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('update', $record);
        
        return view($this->filterEditView('_resources.edit'), $this->filterEditViewData($record, [
            'record' => $record,
            'resourceAlias' => $this->getResourceAlias(),
            'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
            'permissionName' => $this->getPermissionName(),
            'resourceTitle' => $this->getResourceTitle(),
            'viewResources' => $this->getResourcesForView()
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('update', $record);

        $valuesToSave = $this->getValuesToSave($request, $record);
        $request->merge($valuesToSave);
        $this->resourceValidate($request, 'update', $record);

        if ($record->update($this->alterValuesToSave($request, $valuesToSave))) {
            $this->afterCreateActions($request,$record);
            flash()->success('Element successfully updated.');

            if($request->input('clone') && $request->input('clone') == 1){
                $new_record = $record->replicate();
                $new_record->name = $new_record->name . '-clone';
                $this->alterCloneData($request, $record, $new_record);
                $new_record->push();
                return $this->show($new_record->id);

            }else{
                return $this->getRedirectAfterSave($record);
            }
        } else {
            flash()->info('Element was not updated.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $record = $this->getResourceModel()::findOrFail($id);

        $this->authorize('delete', $record);

        if (! $this->checkDestroy($record)) {
            return redirect(route($this->getResourceRoutesAlias().'.index'));
        }

        if ($record->delete()) {
            flash()->success('Element successfully deleted.');
        } else {
            flash()->info('Element was not deleted.');
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    /**
     * Delete all selected records at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = $request->input('ids');// $this->getResourceModel()::whereIn('id', $request->input('ids'))->get();
            
            foreach ($entries as $id) {
                $entity = $this->getResourceModel()::findOrFail($id);
                $entity->delete();
            }
        }
    }

    /**
     * Clone all selected records at once.
     *
     * @param Request $request
     */
    public function massClone(Request $request)
    {
        if ($request->input('ids')) {
            $entries = $request->input('ids');
            $amount = $request->input('clone-amount');

            $relations = $this->getModelRelations();
            
            if($amount > 0){
                foreach ($entries as $id) {
                    //load entity with all relations
                    $entity = $this->getResourceModel()::findOrFail($id);
                    if($entity){

                        $entity->load($relations);                        

                        for( $i=0; $i < $amount; $i++ ){
                            
                            $new_entity = $entity->replicate();
                            $new_entity->name = $new_entity->name.'-clone';
                            $new_entity = $this->alterCloneData($request, $entity, $new_entity);
                            $new_entity->push();

                            $this->afterCloneActions($request, $entity, $new_entity);
                        }
                    }
                }
                flash()->success('Replicated '. $amount . ' '.$this->getResourceTitle());
            }else{
                flash()->error('Amount must be greater than 0');
            }
        }

        return $this->redirectBackTo(route($this->getResourceRoutesAlias().'.index'));
    }

    public function annonymousIndex(Request $request, Company $company){
        
        if(!$company){
            return new HttpResponseException( new Response('Company not found',404));
        }

        $perPage = (int) $request->input('per_page', '');
        $perPage = (is_numeric($perPage) && $perPage > 0 && $perPage <= 100) ? $perPage : 15;
        
        
        $records = $this->getResourceModel()
        ::leftJoin('assets as a', 'a.asseteable_id', '=', 'apartment_assets.id')
        ->leftJoin('entities as ag', 'ag.id', '=', 'a.entity_id')
        ->leftJoin('projects as p', 'p.id', '=', 'ag.project_id')
        ->leftJoin('agents as pg', 'pg.id', '=', 'p.agent_id')
        ->leftJoin('companies as c', 'c.id', '=', 'pg.company_id')
        ->whereIn('c.id', [$company->id])
        ->where('a.asseteable_type', '=', $this->getResourceModel())
        ->paginate($perPage);
        $records->load($this->getModelRelations());
        return $records;

    }
}
