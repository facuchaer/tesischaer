<?php

namespace App;

use App\User;
use App\Models\Company;
use App\Models\Agent;
use App\Models\Project;
use App\Models\Entity;
use App\Models\SimpleText;
use App\Models\SimpleNumber;
use App\Models\SimpleImage;
use App\Models\WebLink;
use App\Models\Email;
use App\Models\Iframe;
use App\Models\ApartmentAsset;
use App\Models\Group;
use App\Models\SimpleSelect;

class Reports
{
    private $totalAgents;
    private $totalSimpleImages;
    private $totalEntities;
    private $totalCompanies;
    private $totalSNumbers;
    private $totalWebLinks;
    private $totalProjects;
    private $totalIframes;
    private $totalSelects;
    private $totalGroups;
    private $totalSTexts;
    private $totalEmails;
    private $totalUsers;
    private $totalApartmentAsset;

    /**
     * @return integer
     */
    public function getTotalUsers()
    {
        if (is_null($this->totalUsers)) {
            $this->totalUsers = User::count();
        }

        return $this->totalUsers;
    }

    public function getTotalCompanies()
    {
        if (is_null($this->totalCompanies)) {
            $this->totalCompanies = Company::count();
        }

        return $this->totalCompanies;
    }

    public function getTotalAgents()
    {
        if (is_null($this->totalAgents)) {
            $this->totalAgents = Agent::count();
        }

        return $this->totalAgents;
    }

    public function getTotalProjects()
    {
        if (is_null($this->totalProjects)) {
            $this->totalProjects = Project::count();
        }

        return $this->totalProjects;
    }

    public function getTotalGroups(){
        if (is_null($this->totalGroups)) {
            $this->totalGroups = Group::count();
        }

        return $this->totalGroups;
    }

    public function getTotalEntities()
    {
        if (is_null($this->totalEntities)) {
            $this->totalEntities = Entity::count();
        }

        return $this->totalEntities;
    }

    public function getTotalSimpleTexts()
    {
        if (is_null($this->totalSTexts)) {
            $this->totalSTexts = SimpleText::count();
        }

        return $this->totalSTexts;
    }

    public function getTotalSimpleNumbers()
    {
        if (is_null($this->totalSNumbers)) {
            $this->totalSNumbers = SimpleNumber::count();
        }

        return $this->totalSNumbers;
    }
    
    public function getTotalWebLinks()
    {
        if (is_null($this->totalWebLinks)) {
            $this->totalWebLinks = WebLink::count();
        }

        return $this->totalWebLinks;
    }

    public function getTotalIframes()
    {
        if (is_null($this->totalIframes)) {
            $this->totalIframes = Iframe::count();
        }

        return $this->totalIframes;
    }

    public function getTotalEmails()
    {
        if (is_null($this->totalEmails)) {
            $this->totalEmails = SimpleSelect::count();
        }

        return $this->totalEmails;
    }

    public function getTotalSelects()
    {
        if (is_null($this->totalSelects)) {
            $this->totalSelects = Email::count();
        }

        return $this->totalSelects;
    }

    public function getTotalSimpleImages()
    {
        if (is_null($this->totalSimpleImages)) {
            $this->totalSimpleImages = SimpleImage::count();
        }

        return $this->totalSimpleImages;
    }

    public function getTotalApartmentAsset()
    {
        if (is_null($this->totalApartmentAsset)) {
            $this->totalApartmentAsset = ApartmentAsset::count();
        }

        return $this->totalApartmentAsset;
    }
}
