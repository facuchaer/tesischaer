<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Asset;

use App\Traits\Models\FillableFields;

class ApartmentAsset extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description','status','price','orientation','rooms_amount','surface',
                        'gen_field_1','gen_field_2','gen_field_3','web_link'];

    const STATUS_NAMES = [
        0 => 'available',
        1 => 'reserved',
        2 => 'rented',
        3 => 'sold'
    ];

    const ORIENTATION_NAMES = [
        0 => 'north',
        1 => 'south',
        2 => 'east',
        3 => 'west'
    ];

    public function getStatusName(){
        return self::STATUS_NAMES[$this->status];
    }

    public function getTypologyName(){
        return self::TYPOLOGIES_NAMES[$this->typology];
    }

    public function getOrientationName(){
        return self::TYPOLOGIES_NAMES[$this->orientation];
    }

    /**
     * Get the project group associated with the project.
     */
    public function asset()
    {
        return $this->morphOne(Asset::class, 'asseteable');
    }

    public function pictures(){
        return $this->morphMany(MultiAssetImage::class, 'imageable');
    }

    public function typology(){
        return $this->belongsTo(Typology::class);
    }

}
