<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Asset;

use App\Traits\Models\FillableFields;

class SimpleText extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description','content'];

    /**
     * Get the project group associated with the project.
     */
    public function asset()
    {
        return $this->morphOne(Asset::class, 'asseteable');
    }
}
