<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Asset;

use App\Traits\Models\FillableFields;

class SimpleImage extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description'];
    

    /**
     * Get the project group associated with the project.
     */
    public function asset()
    {
        return $this->morphOne(Asset::class, 'asseteable');
    }

    /**
     * Get the project group associated with the project.
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

}
