<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Models\FillableFields;

use App\Models\Entity;

class Asset extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    //protected $fillable = ['name','description'];

    /**
     * Get the project group associated with the project.
     */
    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    /**
     * Get the project group associated with the project.
     */
    public function asseteable()
    {
        return $this->morphTo();
    }

}
