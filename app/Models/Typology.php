<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Models\FillableFields;

class Typology extends Model
{

    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description'];
    

    public function apartments(){
        return $this->hasMany(ApartmentAsset::class);
    }

    public function project(){
        return $this->belongsTo(Project::class);
    }
}
