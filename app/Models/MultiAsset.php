<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Asset;

use App\Traits\Models\FillableFields;

class MultiAsset extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    /**
     * Get the project group associated with the project.
     */
    public function assets()
    {
        return $this->morphMany(Asset::class, 'asseteable');
    }
}
