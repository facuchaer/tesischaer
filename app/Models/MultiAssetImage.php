<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Models\FillableFields;

class MultiAssetImage extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];
    
    protected $fillable = ['path'];

    /**
     * Get the project group associated with the project.
     */
    public function imageable()
    {
        return $this->morphTo();
    }
}
