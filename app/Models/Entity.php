<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Models\FillableFields;
use App\Models\Group;
use App\Models\Asset;

class Entity extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];
    
    protected $fillable = ['name','description'];

    /**
     * Get the project group associated with the project.
     */
    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function assets(){
        return $this->hasMany(Asset::class);
    }

}
