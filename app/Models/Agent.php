<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Traits\Models\FillableFields;
use App\Models\Company;
use App\Models\Project;

class Agent extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description'];

    /**
     * Get the company associated with the project group.
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function projects(){
        return $this->hasMany(Project::class);
    }
}
