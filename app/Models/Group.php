<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Models\FillableFields;

use App\Models\Entity;

class Group extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description'];

    /**
     * Get the project group associated with the project.
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function entities(){
        return $this->hasMany(Entity::class);
    }

    public function typologies(){
        return $this->hasMany(Typology::class);
    }
}
