<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;


class Schema extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','name','description','schema'];

}
