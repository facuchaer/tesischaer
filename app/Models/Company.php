<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Agent;
use App\Models\Image;
use App\Traits\Models\FillableFields;
use Illuminate\Database\Eloquent\SoftDeletes;


class Company extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description','address','web','email'];

    
    public function agents(){
        return $this->hasMany(Agent::class);
    }

    /**
     * Get the user contact associated with the company.
     */
    public function contact()
    {
        return $this->belongsTo(User::class);
    }
    /**
     * Get the users for the company.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get the logo associated with the company.
     */
    public function logo()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

}
