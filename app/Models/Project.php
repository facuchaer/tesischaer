<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Models\FillableFields;

use App\Models\Agent;
use App\Models\Entity;

class Project extends Model
{
    use SoftDeletes, FillableFields;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name','description'];

    /**
     * Get the project group associated with the project.
     */
    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function groups(){
        return $this->hasMany(Group::class);
    }
}
