<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\User::class => \App\Policies\UserPolicy::class,
        \App\Models\Asset::class => \App\Policies\AssetPolicy::class,
        \App\Models\Company::class => \App\Policies\CompanyPolicy::class,
        \App\Models\Project::class => \App\Policies\ProjectPolicy::class,
        \App\Models\SimpleText::class => \App\Policies\SimpleTextPolicy::class,
        \App\Models\Entity::class => \App\Policies\EntityPolicy::class,
        \App\Models\SimpleImage::class => \App\Policies\SimpleImagePolicy::class,
        \Spatie\Permission\Models\Role::class => \App\Policies\RolePolicy::class,
        \App\Models\SimpleNumber::class => \App\Policies\SimpleNumberPolicy::class,
        \App\Models\Agent::class => \App\Policies\AgentPolicy::class,
        \Spatie\Permission\Models\Permission::class => \App\Policies\PermissionPolicy::class,
        \App\Models\WebLink::class => \App\Policies\WebLinkPolicy::class,
        \App\Models\Iframe::class => \App\Policies\IframePolicy::class,
        \App\Models\Email::class => \App\Policies\EmailPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            return $user->hasRole('Super Admin') ? true : null;
        });

        Passport::routes();
    }
}
