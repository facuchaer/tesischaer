<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApartmentAsset extends JsonResource
{

    protected function getResourceRelations() : array{
        return [
            'entities' => \App\Http\Resources\ApartmentAsset::collection($this->whenLoaded('entities')),
            'typology' => \App\Http\Resources\Typology::collection($this->whenLoaded('typology')),
            'pictures' => \App\Http\Resources\Typology::collection($this->whenLoaded('typology')),
		];
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $properties = array();
        $reflect = new \ReflectionClass(self::class);
        $props = $reflect->getProperties();
        foreach ($props as $prop) {
            $name = $prop->getName();
            $properties[$name]= $this->$name;
        }
        return array_merge($properties,$this->getResourceRelations());
    }
}
