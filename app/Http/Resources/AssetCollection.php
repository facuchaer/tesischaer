<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AssetCollection extends ResourceCollection
{

    protected function getResourceRelations() : array {
       return [ 'entities' => \App\Http\Resources\Asset::collection($this->whenLoaded('entities')),
		];
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $properties = array();
        $reflect = new \ReflectionClass(self::class);
        $props = $reflect->getProperties();
        foreach ($props as $prop) {
            $name = $prop->getName();
            $properties[$name]= $this->$name;
        }
        return array_merge($properties,$this->getResourceRelations());
    }
}
