<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Project extends JsonResource
{

    protected function getResourceRelations() : array {
       return ['entities' => \App\Http\Resources\Entity::collection($this->whenLoaded('entities')),
		];
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $properties = array();
        $reflect = new \ReflectionClass(self::class);
        $props = $reflect->getProperties();
        foreach ($props as $prop) {
            $name = $prop->getName();
            $properties[$name]= $this->$name;
        }
        return array_merge($properties,$this->getResourceRelations());
    }
}
