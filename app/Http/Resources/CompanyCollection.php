<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyCollection extends ResourceCollection
{

    protected function getResourceRelations() : array {
       return [ 'contact' => \App\User::collection($this->whenLoaded('contact')),
		'agents' => \App\Http\Resources\Agent::collection($this->whenLoaded('agents')),
		'users' => \App\User::collection($this->whenLoaded('users')),
		'logo' => \App\Http\Resources\Image::collection($this->whenLoaded('logo')),
		];
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $properties = array();
        $reflect = new \ReflectionClass(self::class);
        $props = $reflect->getProperties();
        foreach ($props as $prop) {
            $name = $prop->getName();
            $properties[$name]= $this->$name;
        }
        return array_merge($properties,$this->getResourceRelations());
    }
}
