<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\SimpleImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class SimpleImagesController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Image';
    protected $resourceRoutesAlias = 'admin::simple_images';
    protected $permissionName = 'SimpleImage';
    protected $resourceAlias = 'admin.simple_images';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\SimpleImage',
		
    ];

    protected $modelRelations = [
        'asset','image'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\SimpleImage';


}
