<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\WebLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class WebLinksController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Web Links';
    protected $resourceRoutesAlias = 'admin::web_links';
    protected $permissionName = 'WebLink';
    protected $resourceAlias = 'admin.web_links';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\WebLink',
		
    ];

    protected $modelRelations = [
        'asset'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\WebLink';


}
