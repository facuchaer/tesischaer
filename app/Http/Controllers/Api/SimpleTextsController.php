<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\SimpleText;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class SimpleTextsController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Texts';
    protected $resourceRoutesAlias = 'admin::simple_texts';
    protected $permissionName = 'SimpleText';
    protected $resourceAlias = 'admin.simple_texts';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\SimpleText',
		
    ];

    protected $modelRelations = [
        'asset'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\SimpleText';


}
