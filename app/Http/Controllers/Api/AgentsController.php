<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Agent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class AgentsController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Agents';
    protected $resourceRoutesAlias = 'admin::agents';
    protected $permissionName = 'Agent';
    protected $resourceAlias = 'admin.agents';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\Agent',
		
    ];

    protected $modelRelations = [
        'company','projects'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\Agent';


}
