<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Email;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class EmailsController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Emails';
    protected $resourceRoutesAlias = 'admin::emails';
    protected $permissionName = 'Email';
    protected $resourceAlias = 'admin.emails';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\Email',
		
    ];

    protected $modelRelations = [
        'asset'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\Email';


}
