<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\ApartmentAsset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class ApartmentAssetsController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Apartment Asset';
    protected $resourceRoutesAlias = 'admin::apartment_asset';
    protected $permissionName = 'ApartmentAsset';
    protected $resourceAlias = 'admin.apartment_asset';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\WebLink',
		
    ];

    protected $modelRelations = [
        'asset','pictures','typology'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\ApartmentAsset';


}
