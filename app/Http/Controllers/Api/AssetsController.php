<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Asset;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class AssetsController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Asset';
    protected $resourceRoutesAlias = 'admin::assets';
    protected $permissionName = 'Asset';
    protected $resourceAlias = 'admin.assets';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\Asset',
		
    ];

    protected $modelRelations = [
        'entity','asseteable'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\Asset';


}
