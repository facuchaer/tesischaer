<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Iframe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class IframesController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Iframes';
    protected $resourceRoutesAlias = 'admin::iframes';
    protected $permissionName = 'Iframe';
    protected $resourceAlias = 'admin.iframes';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\Iframe',
		
    ];

    protected $modelRelations = [
        'asset'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\Iframe';


}
