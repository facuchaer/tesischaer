<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class CompaniesController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Company';
    protected $resourceRoutesAlias = 'admin::companies';
    protected $permissionName = 'Company';
    protected $resourceAlias = 'admin.companies';

    protected $modelApiRelations = [
        'contact'=>'\App\User',
		'agents'=>'\App\Http\Resources\Agent',
		'users'=>'\App\User',
		'logo'=>'\App\Http\Resources\Image',
		
    ];

    protected $modelRelations = [
        'contact','agents','users','logo'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\Company';


}
