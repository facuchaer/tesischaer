<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Entity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class EntitiesController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Entity';
    protected $resourceRoutesAlias = 'admin::entities';
    protected $permissionName = 'Entity';
    protected $resourceAlias = 'admin.entities';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\Entity',
		
    ];

    protected $modelRelations = [
        'project','asset'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\Entity';


}
