<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\SimpleNumber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class SimpleNumbersController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Numbers';
    protected $resourceRoutesAlias = 'admin::simple_numbers';
    protected $permissionName = 'SimpleNumber';
    protected $resourceAlias = 'admin.simple_numbers';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\SimpleNumber',
		
    ];

    protected $modelRelations = [
        'asset'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\SimpleNumber';


}
