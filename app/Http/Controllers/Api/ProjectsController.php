<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ApiResourceController;

class ProjectsController extends Controller
{

    use ApiResourceController;

    protected $resourceTitle = 'Projects';
    protected $resourceRoutesAlias = 'admin::projects';
    protected $permissionName = 'Project';
    protected $resourceAlias = 'admin.projects';

    protected $modelApiRelations = [
        'entities'=>'\App\Http\Resources\Entity',
		
    ];

    protected $modelRelations = [
        'entities','agent'
    ];

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = 'App\Models\Project';


}
