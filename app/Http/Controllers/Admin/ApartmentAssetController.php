<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;
use App\Models\Entity;
use App\Models\MultiAssetImage;
use App\Models\ApartmentAsset;
use Illuminate\Support\Facades\Auth;
use App\Models\Typology;

class ApartmentAssetController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'asset','images','typology'
    ];

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.apartment_assets';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::apartment_assets';
    protected $permissionName = 'ApartmentAsset';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = ApartmentAsset::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Apartment Assets';

    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                
                'picture_1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'picture_2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'picture_3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'price' => 'required',
                'rooms_amount' => 'required',
                'surface' => 'required'
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                
                'image_1' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'image_2' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'image_3' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'price' => 'required',
                'rooms_amount' => 'required',
                'surface' => 'required'
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * @param $record
     * @return bool
     */
    private function checkDestroy($record)
    {
        return true;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth= 'own')
    {
        $companies = Auth::user()->companies->pluck('id')->toArray();

        if($auth == 'own'){
            return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
                $query->where(function ($query) use ($search) {
                    $query->where('name', 'like', "%$search%");
                });
            })
            ->leftJoin('assets as a', 'a.asseteable_id', '=', 'apartment_assets.id')
            ->leftJoin('entities as ag', 'ag.id', '=', 'a.entity_id')
            ->leftJoin('groups as g', 'g.id', '=', 'ag.group_id')
            ->leftJoin('projects as p', 'p.id', '=', 'g.project_id')
            ->leftJoin('agents as pg', 'pg.id', '=', 'p.agent_id')
            ->leftJoin('companies as c', 'c.id', '=', 'pg.company_id')
            ->whereIn('c.id', $companies)
            ->where('a.asseteable_type', '=', $this->getResourceModel())
            ->paginate($perPage);
        }else{
            return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
                $query->where(function ($query) use ($search) {
                    $query->where('name', 'like', "%$search%");
                });
            })
            ->paginate($perPage);
        }
    }

    private function getResourcesForView(){
        return [
            'entities' => Entity::get()->pluck('name', 'id'),
            'asset_statuses' => ApartmentAsset::STATUS_NAMES,
            'orientation_names' => ApartmentAsset::ORIENTATION_NAMES,
            'typologies_names' => Typology::get()->pluck('name', 'id')
        ];
    }

    private function afterCreateActions(Request $request, $record){
        if($request->input('entity')){
            $entity = Entity::find($request->input('entity'));
            $asset = $entity->assets()->create([]);
            $asset->asseteable()->associate($record)->save();
        }

        if($request->input('typology')){
            $record->typology()->associate($record)->save();

        }
        if ($request->hasFile('picture_1')) {

            $img_prod_gallery = new MultiAssetImage();
            $image_name = uniqid().'.'.\Str::lower($request->picture_1->getClientOriginalExtension());
            $path = '/files/images/apartment_asset/'. $entity->id;
            $img_prod_gallery->path = $path . '/' . $image_name;

            if($request->input('width') && $request->input('height')){
                $img = \Image::make($img_prod_gallery->path)
                        ->resize($request->input('width'), $request->input('height'))
                        ->save($path .'/'. $image_name);
            }else{
                $request->picture_1->move(public_path($path), $image_name);
            }
            $img_prod_gallery->imageable()->associate($record)->save();
        }

        if ($request->hasFile('picture_2')) {

            $img_prod_gallery = new MultiAssetImage();
            $image_name = uniqid().'.'.\Str::lower($request->picture_2->getClientOriginalExtension());
            $path = '/files/images/apartment_asset/'. $entity->id;
            $img_prod_gallery->path = $path . '/' . $image_name;

            if($request->input('width') && $request->input('height')){
                $img = \Image::make($img_prod_gallery->path)
                        ->resize($request->input('width'), $request->input('height'))
                        ->save($path .'/'. $image_name);
            }else{
                $request->picture_2->move(public_path($path), $image_name);
            }
            $img_prod_gallery->imageable()->associate($record)->save();
        }

        if ($request->hasFile('picture_3')) {

            $img_prod_gallery = new MultiAssetImage();
            $image_name = uniqid().'.'.\Str::lower($request->picture_3->getClientOriginalExtension());
            $path = '/files/images/apartment_asset/'. $entity->id;
            $img_prod_gallery->path = $path . '/' . $image_name;

            if($request->input('width') && $request->input('height')){
                $img = \Image::make($img_prod_gallery->path)
                        ->resize($request->input('width'), $request->input('height'))
                        ->save($path .'/'. $image_name);
            }else{
                $request->picture_3->move(public_path($path), $image_name);
            }
            $img_prod_gallery->imageable()->associate($record)->save();
        }
    }

    private function afterEditActions(Request $request, $record){
        if($request->input('entity')){
            $record->asset->entity()->associate($request->input('entity'))->save();
        }else{
            $record->asset->entity()->associate(null)->save();
        }

        if($request->input('typology')){
            $record->typology()->associate($request->input('typology'))->save();
        }else{
            $record->typology()->associate(null)->save();
        }
    }

    /**
     * @param array $data
     * @return array
     */
    private function filterCreateViewData($data = [])
    {
        // Add additional data
        $data['addVarsForView']['formFiles'] = true;

        return $data;
    }

    /**
     * @param $record
     * @param array $data
     * @return array
     */
    private function filterEditViewData($record, $data = [])
    {
        // Add additional data
        $data['addVarsForView']['formFiles'] = true;

        return $data;
    }

    private function afterCloneActions(Request $request, $record, $new_record){
        $asset = $record->asset->entity->assets()->create([]);
        $asset->asseteable()->associate($new_record)->save();
    }
}
