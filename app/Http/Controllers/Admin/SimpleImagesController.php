<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;
use App\Models\Entity;
use App\Models\SimpleImage;
use App\Models\Image;
use Illuminate\Support\Facades\Auth;

class SimpleImagesController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'asset','image'
    ];

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.simple_images';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::simple_images';
    protected $permissionName = 'SimpleImage';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = SimpleImage::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Simple Images';

    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * @param $record
     * @return bool
     */
    private function checkDestroy($record)
    {
        return true;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth= 'own')
    {
        $companies = Auth::user()->companies->pluck('id')->toArray();

        if($auth == 'own'){
            return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
                $query->where(function ($query) use ($search) {
                    $query->where('name', 'like', "%$search%");
                });
            })
            ->leftJoin('assets as a', 'a.asseteable_id', '=', 'simple_images.id')
            ->leftJoin('entities as ag', 'ag.id', '=', 'a.entity_id')
            ->leftJoin('groups as g', 'g.id', '=', 'ag.group_id')
            ->leftJoin('projects as p', 'p.id', '=', 'g.project_id')
            ->leftJoin('agents as pg', 'pg.id', '=', 'p.agent_id')
            ->leftJoin('companies as c', 'c.id', '=', 'pg.company_id')
            ->whereIn('c.id', $companies)
            ->where('a.asseteable_type', '=', $this->getResourceModel())
            ->paginate($perPage);
        }else{
            return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
                $query->where(function ($query) use ($search) {
                    $query->where('name', 'like', "%$search%");
                });
            })
            ->paginate($perPage);
        }
    }

    private function getResourcesForView(){
        return [
            'entities' => Entity::get()->pluck('name', 'id')
        ];
    }

    private function afterCreateActions(Request $request, $record){
        if($request->input('entity')){
            $entity = Entity::find($request->input('entity'));
            $asset = $entity->assets()->create([]);
            $asset->asseteable()->associate($record)->save();
        }

        if ($request->hasFile('image')) {

            $img_prod_gallery = new Image();
            $image_name = uniqid().'.'.\Str::lower($request->image->getClientOriginalExtension());
            $path = '/files/images/simple/'. $entity->id;
            $img_prod_gallery->path = $path . '/' . $image_name;

            if($request->input('width') && $request->input('height')){
                $img = \Image::make($img_prod_gallery->path)
                        ->resize($request->input('width'), $request->input('height'))
                        ->save($path .'/'. $image_name);
            }else{
                $request->image->move(public_path($path), $image_name);
            }
            $img_prod_gallery->imageable()->associate($record)->save();
        }
    }

    private function afterEditActions(Request $request, $record){
        if($request->input('entity')){
            $record->asset->entity()->associate($request->input('entity'))->save();
        }else{
            $record->asset->entity()->associate(null)->save();
        }
    }

    /**
     * @param array $data
     * @return array
     */
    private function filterCreateViewData($data = [])
    {
        // Add additional data
        $data['addVarsForView']['formFiles'] = true;

        return $data;
    }

    /**
     * @param $record
     * @param array $data
     * @return array
     */
    private function filterEditViewData($record, $data = [])
    {
        // Add additional data
        $data['addVarsForView']['formFiles'] = true;

        return $data;
    }

    private function afterCloneActions(Request $request, $record, $new_record){
        $asset = $record->asset->entity->assets()->create([]);
        $asset->asseteable()->associate($new_record)->save();
    }
}
