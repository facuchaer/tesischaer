<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;

class PermissionsController extends Controller
{

    use ResourceController;

    protected $modelRelations = [
        'roles','users'
    ];
    /**
     * @var string
     */
    protected $resourceAlias = 'admin.permissions';
    protected $permissionName = 'Permission';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::permissions';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Permission::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Permissions';



    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth= 'own')
    {
        return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        })->paginate($perPage);
    }

    private function getResourcesForView(){
        return [];
    }

    /**
     * Clone all selected records at once.
     *
     * @param Request $request
     */
    public function administer_permissions(Request $request){
        //TODO ---- GRID LIKE ROLES EDIT.
        //              role_1  role_2  role_3
        //permission_1     y              y  
        //permission_2     y      y

        // if(Auth::user()->hasRole('Super Admin')){

        //     $permissions = Permission::get()->pluck('name', 'id');
        //     $roles_amount = Role::count();
        //     $roles= Role::get();  

        //     return view($this->filterEditView('admin.permissions.administer_permissions'), [
        //         'records' => $permissions,
        //         'roles' => $roles,
        //         'resourceAlias' => $this->getResourceAlias(),
        //         'resourceRoutesAlias' => $this->getResourceRoutesAlias(),
        //         'resourceTitle' => $this->getResourceTitle(),
        //         'viewResources' => $this->getResourcesForView()
        //     ]);

        // }
    }
}
