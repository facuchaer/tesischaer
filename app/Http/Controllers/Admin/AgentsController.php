<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;

use App\Models\Agent;
use App\Models\Company;

use Illuminate\Support\Facades\Auth;

class AgentsController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'company','projects'
    ];

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.agents';
    protected $permissionName = 'Agent';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::agents';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Agent::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Agents';



    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth='own')
    {
        $retQuery = $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        });
        
        if($auth == 'own'){
            $agents = [];
            foreach (Auth::user()->companies as $company) {
                
                $agents = array_merge( $agents , $company->agents->pluck('id')->toArray());
            }
            $retQuery->whereIn('id', $agents);
            
        }

        return $retQuery->paginate($perPage);
    }

    private function getResourcesForView(){
        return [
            'companies' => Company::get()->pluck('name', 'id')
        ];
    }

    private function afterCreateActions(Request $request, $record){
        if($request->input('company')){
            $company = Company::find($request->input('company'));
            $company->agents()->save($record);
        }
    }

    private function afterEditActions(Request $request, $record){
        if($request->input('company')){
            $record->company()->associate($request->input('company'))->save();
        }else{
            $record->company()->associate(null)->save();
        }
    }
}
