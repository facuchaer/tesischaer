<?php

namespace App\Http\Controllers\Admin;

use App\Models\Typology;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;
use Illuminate\Support\Facades\Auth;
use App\Models\Project;

class TypologiesController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'apartments','project'
    ];

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.typologies';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::typologies';
    protected $permissionName = 'Typologies';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Typology::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Typologies';

    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                'description' => 'required'
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                'description' => 'required'
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * @param $record
     * @return bool
     */
    private function checkDestroy($record)
    {
        return true;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth= 'own')
    {
        $retQuery = $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        });
        
        if($auth == 'own'){
            $typologies = [];
            foreach (Auth::user()->companies as $company) {
                $typologies = array_merge( $typologies , $company->typologies->pluck('id')->toArray());
            }
            $retQuery->whereIn('id', $typologies);
            
        }

        return $retQuery->paginate($perPage);
    }

    private function getResourcesForView(){
        return [
            'projects' => Project::get()->pluck('name', 'id')
        ];
    }

    private function afterCreateActions(Request $request, $record){

        if($request->input('project')){
            $record->project()->associate($request->input('project'))->save();
        }
        
    }

    private function afterEditActions(Request $request, $record){
        if($request->input('project')){
            $record->project()->associate($request->input('project'))->save();
        }else{
            $record->asset->project()->associate(null)->save();
        }
    }

    /**
     * @param array $data
     * @return array
     */
    private function filterCreateViewData($data = [])
    {
        return $data;
    }

    /**
     * @param $record
     * @param array $data
     * @return array
     */
    private function filterEditViewData($record, $data = [])
    {
        return $data;
    }

    private function afterCloneActions(Request $request, $record, $new_record){
    }
}
