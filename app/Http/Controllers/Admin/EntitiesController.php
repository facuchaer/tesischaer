<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;

use App\Models\Entity;
use App\Models\Group;

use Illuminate\Support\Facades\Auth;

class EntitiesController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'group','assets'
    ];

    /**
     * @var string
     * @ alias del recurso.
     * @ nombre del permiso.
     * @ alias de ruta.
     * @ titulo del recurso.
     */
    protected $resourceAlias = 'admin.entities';
    protected $permissionName = 'Entities';
    protected $resourceRoutesAlias = 'admin::entities';
    protected $resourceModel = Entity::class;
    protected $resourceTitle = 'Entity';



    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth= 'own')
    {
        $retQuery = $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        });
        
        if($auth == 'own'){
            $entities = [];
            foreach (Auth::user()->companies as $company) {
                foreach ($company->agents as $agent) {
                    foreach ($agent->projects as $project) {
                        foreach ($project->groups as $group) {
                            $entities = array_merge( $entities , $group->entities->pluck('id')->toArray());   
                        }
                    }
                }
            }
            $retQuery->whereIn('id', $entities);
        }

        return $retQuery->paginate($perPage);
    }

    private function getResourcesForView(){
        return [
            'groups' => Group::get()->pluck('name', 'id')
        ];
    }

    private function afterCreateActions(Request $request, $record){
        if($request->input('group')){
            $group = Group::find($request->input('group'));
            $group->entities()->save($record);
        }
    }

    private function afterEditActions(Request $request, $record){
        if($request->input('group')){
            $record->group()->associate($request->input('group'))->save();
        }else{
            $record->group()->associate(null)->save();
        }
    }



}
