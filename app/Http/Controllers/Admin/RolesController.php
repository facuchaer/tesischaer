<?php

namespace App\Http\Controllers\Admin;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;

class RolesController extends Controller
{

    use ResourceController;

    protected $modelRelations = [
        'permissions','users'
    ];

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.roles';
    protected $permissionName = 'Role';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::roles';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Role::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Roles';

 
   /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

     /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();

        $permissions = $request->input('permission') ? $request->input('permission') : [];

        if ($record){
            //es un update
            $record->syncPermissions($permissions);
        }
        
        //saco los permisos
        unset($values['permission']);

        return $values;
    }

    private function afterCreateActions(Request $request, $record){

        $permissions = $request->input('permission') ? $request->input('permission') : [];

        if(count($permissions) > 0){
            $record->givePermissionTo($permissions);    
        }
    }

    private function afterEditActions(Request $request, $record){

        $permissions = $request->input('permission') ? $request->input('permission') : [];

        if(count($permissions) > 0){
            $record->syncPermissions($permissions);    
        }
    
    }

    private function alterValuesToSave(Request $request, $values)
    {
        
        return $values;
    }
    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth= 'own')
    {
        return $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        })->paginate($perPage);
    }

    private function getResourcesForView(){
        return [
            'permissions' => Permission::get()->pluck('name', 'name')
        ];
    }

}
