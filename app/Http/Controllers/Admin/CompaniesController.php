<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Company;

use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompaniesController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'contact','agents','users','logo'
    ];
    protected $resourceAlias = 'admin.companies';
    protected $permissionName = 'Company';
    protected $resourceRoutesAlias = 'admin::companies';
    protected $resourceTitle = 'Companies';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Company::class;

   /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                
                'address' => 'required',
                'web' => 'required',
                'email' => 'required',
                'contact' => 'required'
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                
                'address' => 'required',
                'web' => 'required',
                'email' => 'required',
                'contact' => 'required'
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

     /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    /**
     * After create actions
     */
    private function afterCreateActions(Request $request, $record){
        if($request->input('contact')){
            $contact = User::find($request->input('contact'));
            $contact->owns()->save($record);
        }
        if($request->input('users') && count($request->input('users')) > 0){
            $record->users()->sync($request->input('users'));
        }else{
            $record->users()->sync([]);
        }
    }   

    private function afterEditActions(Request $request, $record){
        if($request->input('contact')){
            $contact = User::find($request->input('contact'));
            $contact->owns()->save($record);
        }else{
            $contact->owns()->save(null);
        }
        if($request->input('users') && count($request->input('users')) > 0){
            $record->users()->sync($request->input('users'));
        }else{
            $record->users()->sync([]);
        }
    }

        /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null , $auth='own')
    {
        $retQuery = $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        });

        if($auth == 'own'){
            $retQuery->whereIn('id', Auth::user()->companies->pluck('id'));
            
        }

        return $retQuery->paginate($perPage);
    }

    /**
     * Resources exposed to view
     */
    private function getResourcesForView(){

        return [
            'users' => User::get()->pluck('name', 'id')
        ];
    }

    /**
     * Hook after clone, for saving relationships
     */
    private function afterCloneActions(Request $request, $record, $new_record){
 
        $new_record->users()->sync($record->users);       
        
    }
}
