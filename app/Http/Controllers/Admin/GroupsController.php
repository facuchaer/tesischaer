<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;

use App\Models\Group;
use App\Models\Project;

use Illuminate\Support\Facades\Auth;


class GroupsController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'project','entities'
    ];

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.groups';
    protected $permissionName = 'Group';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::groups';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Group::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Groups';



    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth='own')
    {
        $retQuery = $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        });
        
        if($auth == 'own'){
            $groups = [];
            foreach (Auth::user()->companies as $company) {
                foreach ($company->agents as $agent) {
                    foreach($agent->projects as $project){
                        $groups = array_merge( $groups , $project->groups->pluck('id')->toArray());   
                    }
                }
            }
            $retQuery->whereIn('id', $groups);
            
        }

        return $retQuery->paginate($perPage);
    }

    private function getResourcesForView(){
        return [
            'projects' => Project::get()->pluck('name', 'id')
        ];
    }

    private function afterCreateActions(Request $request, $record){
        if($request->input('project')){
            $project = Project::find($request->input('project'));
            $project->groups()->save($record);
        }
    }

    private function afterEditActions(Request $request, $record){
        if($request->input('project')){
            $record->project()->associate($request->input('project'))->save();
        }else{
            $record->project()->associate(null)->save();
        }
    }
}
