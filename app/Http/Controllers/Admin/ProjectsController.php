<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\Controllers\ResourceController;

use App\Models\Agent;
use App\Models\Project;

use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    use ResourceController;

    protected $modelRelations = [
        'agent','groups'
    ];

    /**
     * @var string
     */
    protected $resourceAlias = 'admin.projects';
    protected $permissionName = 'Project';

    /**
     * @var string
     */
    protected $resourceRoutesAlias = 'admin::projects';

    /**
     * Fully qualified class name
     *
     * @var string
     */
    protected $resourceModel = Project::class;

    /**
     * @var string
     */
    protected $resourceTitle = 'Projects';



    /**
     * Used to validate store.
     *
     * @return array
     */
    private function resourceStoreValidationData()
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

        /**
     * Used to validate update.
     *
     * @param $record
     * @return array
     */
    private function resourceUpdateValidationData($record)
    {
        return [
            'rules' => [
                'name' => 'required',
                
            ],
            'messages' => [],
            'attributes' => [],
        ];
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param null $record
     * @return array
     */
    private function getValuesToSave(Request $request, $record = null)
    {
        $values = $request->all();
        return $values;
    }

    private function alterValuesToSave(Request $request, $values)
    {
        return $values;
    }

    /**
     * Retrieve the list of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $perPage
     * @param string|null $search
     * @return \Illuminate\Support\Collection
     */
    private function getSearchRecords(Request $request, $perPage = 15, $search = null, $auth='own')
    {
        $retQuery = $this->getResourceModel()::when(! empty($search), function ($query) use ($search) {
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', "%$search%");
            });
        });
        
        if($auth == 'own'){
            $projects = [];
            foreach (Auth::user()->companies as $company) {
                foreach ($company->agents as $agent) {
                    $projects = array_merge( $projects , $agent->projects->pluck('id')->toArray());   
                }
            }
            $retQuery->whereIn('id', $projects);
            
        }

        return $retQuery->paginate($perPage);
    }

    private function getResourcesForView(){
        return [
            'agents' => Agent::get()->pluck('name', 'id')
        ];
    }

    private function afterCreateActions(Request $request, $record){
        if($request->input('agent')){
            $agent = Agent::find($request->input('agent'));
            $agent->projects()->save($record);
        }
    }

    private function afterEditActions(Request $request, $record){
        if($request->input('agent')){
            $record->agent()->associate($request->input('agent'))->save();
        }else{
            $record->agent()->associate(null)->save();
        }
    }
}
