<div class="col-md-7">

    @if($record->id)
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Id</label>
            <span class="form-control">{{$record->id}}</span>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
    @endif

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" required>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('description') ? ' has-error' : '' }}">
            <label for="description">Description</label>
            <textarea type="description" class="form-control" name="description" placeholder="description">{{ old('description', $record->description) }}
            </textarea>

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5">
            <fieldset id="buildyourform">
                <legend>Build your own form!</legend>
                @if (!empty($record->schema))
                    @foreach(json_decode($record->schema) as $key => $values)
                        <div class="form-inline margin-t-5 margin-b-5" id="field{{$key}}" data-idx="{{$key}}">
                            <input name="schema_fields[{{$key}}][field_label]" placeholder="label" value="{{$values->field_label}}" type="text" class="fieldname form-control margin-r-5 margin-l-5">
                            <select name="schema_fields[{{$key}}][field_type]" class="fieldtype form-control" >
                                <option value="number" {{$values->field_type == "number" ? 'selected' : ''}}>Number</option>
                                <option value="text" {{$values->field_type == "text" ? 'selected' : ''}}>Text</option> 
                                <option value="weblink" {{$values->field_type == "weblink" ? 'selected' : ''}}>Web Link</option> 
                                <option value="email" {{$values->field_type == "email" ? 'selected' : ''}}>Email</option> 
                                <option value="iframe" {{$values->field_type == "iframe" ? 'selected' : ''}}>Iframe</option> 
                                <option value="image" {{$values->field_type == "image" ? 'selected' : ''}}>Image</option> 
                                <option value="select" {{$values->field_type == "select" ? 'selected' : ''}}>Select</option> 
                            </select>
                            <input type="button" class="remove-button form-control btn btn-xs btn-danger margin-r-5 margin-l-5" value="-">
                            @if($values->field_type == 'select')
                                <label>Values , separated</label>
                                <textarea name="schema_fields[{{$key}}][field_values]" id="select-values{{$key}}" class="form-control">{{ $values->field_values }}</textarea>
                            @endif
                        </div>
                    @endforeach
                @endif
            </fieldset>
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5">
            <button class="btn btn-sm btn-info margin-r-5 margin-l-5" id="preview">
                <i class="fa fa-form"></i> <span>Preview</span>
            </button>
            <button class="btn btn-sm btn-info margin-r-5 margin-l-5" id="add">
                <i class="fa fa-plus"></i> <span>Add</span>
            </button>
        </div>
    </div>

    <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('schema') ? ' has-error' : '' }}">
                <label for="schema">Schema</label>
                <textarea disabled type="schema" class="form-control" name="schema" placeholder="schema">{{ old('schema', $record->schema) }}
                </textarea>
    
                @if ($errors->has('schema'))
                    <span class="help-block">
                        <strong>{{ $errors->first('schema') }}</strong>
                    </span>
                @endif
            </div>
            <!-- /.form-group -->
        </div>
</div>

<div class="col-md-5">
        <div id="custom-form">
        </div>
</div>

<script>
var text_form = '<input type="text" class="form-control" id="dummy_replace_id" name="dummy_replace_name" placeholder="Text">';
var number_form = '<input type="number" id="dummy_replace_id" name="dummy_replace_name" type="number" class="form-control" placeholder="Number">';
var weblink_form = '<input type="url" class="form-control" id="dummy_replace_id" name="dummy_replace_name" placeholder="link">';
var email_form = '<input id="dummy_replace_id" name="dummy_replace_name" type="email" class="form-control" placeholder="email">';
var iframes_form = '<input type="url" id="dummy_replace_id" name="dummy_replace_name" class="form-control" placeholder="iframe link">';
var src = "{{ env('APP_URL')}}" + "/adminlte/img/avatar_1.png";
var images_form = '<span> <img style="max-width:150px; border:1px solid gray; border-radius:2%;" src="'+src+'" alt="image"></span><input type="file" class="form-control" id="dummy_replace_id" name="dummy_replace_name" >';
var select_form = '<select id="dummy_replace_id" name="dummy_replace_name" class="form-control" placeholder="iframe link"> dummy_replace_options </select>';
window.onload = function(){
    $(document).ready(function() {
        $(".remove-button").click(function(){
            $(this).parent().remove();
        });

        $("#add").click(function(evt) {
            evt.preventDefault();
            var lastField = $("#buildyourform div:last");
            var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
            var fieldWrapper = $("<div class=\"form-inline margin-t-5 margin-b-5\" id=\"field" + intId + "\"/>");
            fieldWrapper.data("idx", intId);
            var fName = $("<input name=\"schema_fields["+intId+"][field_label]\" placeholder=\"label\" type=\"text\" class=\"fieldname form-control margin-r-5 margin-l-5\" />");
            var fType = $("<select name=\"schema_fields["+intId+"][field_type]\" class=\"fieldtype form-control\"><option value=\"number\">Number</option><option value=\"text\">Text</option> <option value=\"weblink\">Web Link</option> <option value=\"email\">Email</option> <option value=\"iframe\">Iframe</option> <option value=\"image\">Image</option> <option value=\"select\">Select</option> </select>");
            fType.change(function(){
                var me = $(this);
                if(me.val() == 'select'){
                    if(!ta){
                        ta = $("<label>Values , separated</label><textarea name=\"schema_fields["+intId+"][field_values]\" id=\"select-values"+intId+"\" class=\"form-control\"></textarea>");
                        me.parent().append(ta);
                    }
                }else{
                    var ta = $('#select-values'+intId);
                    if(ta){
                        ta.parent().remove();
                    }
                }
            });
            var removeButton = $("<input type=\"button\" class=\"remove-button form-control btn btn-xs btn-danger margin-r-5 margin-l-5\" value=\"-\" />");
            removeButton.click(function() {
                $(this).parent().remove();
            });
            fieldWrapper.append(fName);
            fieldWrapper.append(fType);
            fieldWrapper.append(removeButton);
            $("#buildyourform").append(fieldWrapper);
        });

        function getSelectOptions(vals){
            var ret = '';
            var parts = vals.split(',');
            for(var i=0 ; i<parts.length; i++){
                ret += '<option value=\"'+i+'\">'+i +'|' +parts[i]+ '</option>';
            }
            return ret;
        }

        $("#preview").click(function(evt) {
            evt.preventDefault();
            $("#yourform").remove();
            var fieldSet = $("<fieldset id=\"yourform\"><legend>Your Form</legend></fieldset>");
            $("#buildyourform div").each(function() {

                var id = $(this).attr("id").replace("field","");
                var uniqueid=  "input" + id;

                var label = $(this).find("input.fieldname").first().val();
                var input = '<div class="col-md-12"><div class="form-group margin-b-5 margin-t-5">';
                input += '<label for="' + uniqueid + '">' + label + "</label>";
                

                switch ($(this).find("select.fieldtype").first().val()) {
                    case "number":
                        input += number_form;
                        break;
                    case "text":
                        input += text_form;
                        break;
                    case "weblink":
                        input += weblink_form;
                        break;    
                    case "email":
                        input += email_form;
                        break;  
                    case "iframe":
                        input += iframes_form;
                        break;  
                    case "image":
                        input += images_form;
                        break;  
                    case "select":
                        input += select_form.replace(/dummy_replace_options/gi, getSelectOptions($('#select-values'+id).val()));
                        break;  
                }

                input = input.replace(/dummy_replace_id/gi,id).replace(/dummy_replace_name/gi,id) + '</div></div>';
                fieldSet.append(input);
            });
            $("#custom-form").append(fieldSet);
        });
    });
}
</script>

