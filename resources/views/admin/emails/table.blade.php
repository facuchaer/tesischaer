<div class="table-responsive list-records">
    <table class="table table-hover table-bordered {{ count($records) > 0 ? 'datatable' : '' }} dt-select">
        <thead>
            <th style="width: 10px;"><button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o"></i></button></th>
            <th>#</th>
            <th>@lang('global.asset.fields.name')</th>
            <th>@lang('global.asset.fields.entity')</th>
            {{-- <th>@lang('global.asset.fields.project')</th>
            <th>@lang('global.asset.fields.agent')</th>
            <th>@lang('global.asset.fields.company')</th> --}}
            <th style="width: 120px;">@lang('global.project.actions')</th>
        </thead>
        <tbody> 
            @foreach ($records as $record)
            <?php
                
                $editLink = route($resourceRoutesAlias.'.edit', $record->id);
                $deleteLink = route($resourceRoutesAlias.'.destroy', $record->id);
                $formId = 'formDeleteModel_'.$record->id;
                $formIdImpersonate = 'impersonateForm_'.$record->id;
                 
            ?>
            <tr>  

                <!-- TABLE DATA -->

                <td data-entry-id="{{ $record->id }}"><input type="checkbox" name="ids[]" value="{{ $record->id }}" class="square-blue"></td>
                <td>{{ $record->id }}</td>
                <td>{{ $record->name }}</td>
                <td>
                        {{ $record->asset && $record->asset->entity ? $record->asset->entity->name : '' }}
                </td>
                
                    {{-- {{ $record->agent->company ? $record->agent->company->name : '' }} --}}
                
                    {{-- {{ $record->agent->company ? $record->agent->company->name : '' }} --}}
                
                    {{-- {{ $record->agent->company ? $record->agent->company->name : '' }} --}}
                

                <!-- END TABLE DATA-->


                <!-- we will also add show, edit, and delete buttons -->
                <td class="actions">
                    <div class="btn-group">
                        @if ($canImpersonate)
                            <a href="#" class="btn btn-warning btn-sm"
                               onclick="event.preventDefault(); document.getElementById('{{$formIdImpersonate}}').submit();"
                            >
                                <i class="fa fa-user-secret"></i>
                            </a>
                            <form id="{{$formIdImpersonate}}" action="{{ route('impersonate', $record->id) }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @endif
                        @if ($canUpdate)
                            <a href="{{ $editLink }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                        @endif
                        @if ($canDelete)
                            <a href="#" class="btn btn-danger btn-sm btnOpenerModalConfirmModelDelete"
                               data-form-id="{{ $formId }}"><i class="fa fa-trash-o"></i></a>
                        @endif
                    </div>
                    @if ($canDelete)
                        <!-- Delete Record Form -->
                        <form id="{{ $formId }}" action="{{ $deleteLink }}" method="POST"
                              style="display: none;" class="hidden form-inline">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger">@lang('global.project.actions.delete')</button>
                        </form>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>