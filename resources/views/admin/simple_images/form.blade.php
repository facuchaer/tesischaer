<div class="col-md-7">
    @if($record->id)
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Id</label>
            <span class="form-control">{{$record->id}}</span>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
    @endif

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" required>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('description') ? ' has-error' : '' }}">
            <label for="description">Description</label>
            <textarea type="description" class="form-control" name="description" placeholder="description">{{ old('description', $record->description) }}
            </textarea>

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('image') ? ' has-error' : '' }}">
            @if($record->image)
                <span> <img style="max-width:150px; border:1px solid gray; border-radius:2%;" src="{{ $record->image ? env('APP_URL').$record->image->path : '' }}" alt="image"></span>
            @endif
            {!! Form::label('image', 'Image', ['class' => 'control-label']) !!}
            {!! Form::file('image', ['placeholder' => 'Select image']) !!}
            <p class="help-block"></p>
            @if($errors->has('image'))
                <p class="help-block">
                    {{ $errors->first('image') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('entity') ? ' has-error' : '' }}">
            {!! Form::label('entity', 'Entity', ['class' => 'control-label']) !!}
            {!! Form::select('entity', $viewResources['entities'], ($record->asset && $record->asset->entity )? $record->asset->entity->id : null, ['class' => 'form-control select2', 'placeholder' => 'Select Value']) !!}
            <p class="help-block"></p>
            @if($errors->has('entity'))
                <p class="help-block">
                    {{ $errors->first('entity') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->
</div>
<!-- /.col-md-7 -->