{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Page Title --}}
@section('page-title', 'Admin')

{{-- Page Subtitle --}}
@section('page-subtitle', 'Control panel')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('admin') !!}
@endsection

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{ $reports->getTotalUsers() }}</h3>
                    <p>Users</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::users.index') }}" class="small-box-footer">List Users <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{ $reports->getTotalCompanies() }}</h3>
                    <p>Companies</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-building" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::companies.index') }}" class="small-box-footer">List Companies <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $reports->getTotalAgents() }}</h3>
                    <p>Agents</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-handshake-o" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::agents.index') }}" class="small-box-footer">List Agents<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{ $reports->getTotalProjects() }}</h3>
                    <p>Project</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-file" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::projects.index') }}" class="small-box-footer">List Projects<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-olive">
                <div class="inner">
                    <h3>{{ $reports->getTotalGroups() }}</h3>
                    <p>Groups</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::groups.index') }}" class="small-box-footer">List Groups<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <div class="inner">
                    <h3>{{ $reports->getTotalEntities() }}</h3>
                    <p>Entities</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-object-group" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::entities.index') }}" class="small-box-footer">List Entities<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalSimpleNumbers() }}</h3>
                    <p>Field > Number</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-percent" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::simple_numbers.index') }}" class="small-box-footer">List Numbers<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalSimpleTexts() }}</h3>
                    <p>Field > Text</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-font" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::simple_texts.index') }}" class="small-box-footer">List Texts<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalWebLinks() }}</h3>
                    <p>Field > Links</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-link" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::web_links.index') }}" class="small-box-footer">List Links<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalIframes() }}</h3>
                    <p>Field > Iframes</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-external-link" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::iframes.index') }}" class="small-box-footer">List Iframes<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalEmails() }}</h3>
                    <p>Field > Emails</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::emails.index') }}" class="small-box-footer">List Emails<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalSelects() }}</h3>
                    <p>Field > Selects</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-list-ul" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::emails.index') }}" class="small-box-footer">List Selects<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
                <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalSimpleImages() }}</h3>
                    <p>Field > Images</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::simple_images.index') }}" class="small-box-footer">List Images<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->

        <div class="col-lg-3 col-xs-6">
                <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{ $reports->getTotalApartmentAsset() }}</h3>
                    <p>Field > Apartment</p>
                </div>
                <div class="icon smaller">
                    <i class="fa fa-home" aria-hidden="true"></i>
                </div>
                <a href="{{ route('admin::apartment_assets.index') }}" class="small-box-footer">List Apartments<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')

@endsection
