<div class="col-md-7">

    @if($record->id)
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Id</label>
            <span class="form-control">{{$record->id}}</span>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
    @endif

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" required>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('description') ? ' has-error' : '' }}">
            <label for="description">Description</label>
            <textarea type="description" class="form-control" name="description" placeholder="description">{{ old('description', $record->description) }}
            </textarea>

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->
    <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5{{ $errors->has('agent') ? ' has-error' : '' }}">
                {!! Form::label('agent', 'Agent', ['class' => 'control-label']) !!}
                {!! Form::select('agent', $viewResources['agents'], $record->agent()->pluck('id','id'), ['class' => 'form-control select2', 'placeholder' => 'Select Value']) !!}
                <p class="help-block"></p>
                @if($errors->has('agent'))
                    <p class="help-block">
                        {{ $errors->first('agent') }}
                    </p>
                @endif
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->

</div>

