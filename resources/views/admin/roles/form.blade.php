<div class="col-md-7">

    @if($record->id)
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Id</label>
            <span class="form-control">{{$record->id}}</span>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
    @endif    
    
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" {{$record->name ? 'disabled': 'required'}}>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->


    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('permission') ? ' has-error' : '' }}">
            <label for="permission">Permissions</label>
            {!! Form::select('permission[]', $viewResources['permissions'], old('permission') ? old('permission') : $record->permissions()->pluck('name', 'name'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
            <p class="help-block"></p>
            @if($errors->has('permission'))
                <p class="help-block">
                    {{ $errors->first('permission') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

</div>
<!-- /.col-md-7 -->
