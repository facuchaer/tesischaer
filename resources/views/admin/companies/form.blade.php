<div class="col-md-7">

    @if($record->id)
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Id</label>
            <span class="form-control">{{$record->id}}</span>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
    @endif
    
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" required>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">Email *</label>
            <input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email', $record->email) }}" required>

            @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('address') ? ' has-error' : '' }}">
            <label for="address">Address *</label>
            <input type="address" class="form-control" name="address" placeholder="Address" value="{{ old('address', $record->address) }}" required>

            @if ($errors->has('address'))
                <span class="help-block">
                    <strong>{{ $errors->first('address') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('web') ? ' has-error' : '' }}">
            <label for="web">Web</label>
            <input type="web" class="form-control" name="web" placeholder="Web" value="{{ old('address', $record->web) }}" required>

            @if ($errors->has('web'))
                <span class="help-block">
                    <strong>{{ $errors->first('web') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('logo') ? ' has-error' : '' }}">
            {!! Form::label('logo', 'Logo', ['class' => 'control-label']) !!}
            {!! Form::file('logo', ['placeholder' => 'Select logo']) !!}
            <p class="help-block"></p>
            @if($errors->has('logo'))
                <p class="help-block">
                    {{ $errors->first('logo') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->
    
    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('contact') ? ' has-error' : '' }}">
            {!! Form::label('contact', 'Contact', ['class' => 'control-label']) !!}
            {!! Form::select('contact', $viewResources['users'], old('contact') ? old('contact') : $record->contact()->pluck('id','id'), ['class' => 'form-control select2', 'placeholder' => 'Select Value']) !!}
            <p class="help-block"></p>
            @if($errors->has('contact'))
                <p class="help-block">
                    {{ $errors->first('contact') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('users') ? ' has-error' : '' }}">
            <label for="users">Users</label>
            {!! Form::select('users[]', $viewResources['users'], old('users') ? old('users') : $record->users()->pluck('users.id', 'users.id'), ['class' => 'form-control select2', 'multiple' => 'multiple']) !!}
            <p class="help-block"></p>
            @if($errors->has('users'))
                <p class="help-block">
                    {{ $errors->first('users') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

</div>
<!-- /.col-md-7 -->