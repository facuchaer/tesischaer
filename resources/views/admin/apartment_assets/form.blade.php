<div class="col-md-5">

    @if($record->id)
        <div class="col-md-12">
            <div class="form-group margin-b-5 margin-t-5">
                <label for="name">Id</label>
            <span class="form-control">{{$record->id}}</span>
            </div>
            <!-- /.form-group -->
        </div>
        <!-- /.col-md-12 -->
    @endif

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Name *</label>
            <input type="text" class="form-control" name="name" placeholder="Name" value="{{ old('name', $record->name) }}" required>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('description') ? ' has-error' : '' }}">
            <label for="description">Description</label>
            <textarea type="description" class="form-control" name="description" placeholder="description">{{ old('description', $record->description) }}
            </textarea>

            @if ($errors->has('description'))
                <span class="help-block">
                    <strong>{{ $errors->first('description') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('price') ? ' has-error' : '' }}">
            <label for="price">Price *</label>
            <input type="number" min="1" step="any" class="form-control" name="price" placeholder="price" value="{{ old('price', $record->price) }}" required>

            @if ($errors->has('price'))
                <span class="help-block">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('rooms_amount') ? ' has-error' : '' }}">
            <label for="rooms_amount">Rooms *</label>
            <input type="number" min="0" step="1" class="form-control" name="rooms_amount" placeholder="rooms_amount" value="{{ old('rooms_amount', $record->rooms_amount) }}" required>

            @if ($errors->has('rooms_amount'))
                <span class="help-block">
                    <strong>{{ $errors->first('rooms_amount') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('surface') ? ' has-error' : '' }}">
            <label for="surface">Surface</label>
            <input type="number" min="0" step="any" class="form-control" name="surface" placeholder="surface" value="{{ old('surface', $record->surface) }}" required>

            @if ($errors->has('surface'))
                <span class="help-block">
                    <strong>{{ $errors->first('surface') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('gen_field_1') ? ' has-error' : '' }}">
            <label for="gen_field_1">Field 1</label>
            <input type="text" class="form-control" name="gen_field_1" placeholder="text field" value="{{ old('gen_field_1', $record->gen_field_1) }}">

            @if ($errors->has('gen_field_1'))
                <span class="help-block">
                    <strong>{{ $errors->first('gen_field_1') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('gen_field_2') ? ' has-error' : '' }}">
            <label for="gen_field_2">Field 2</label>
            <input type="text" class="form-control" name="gen_field_2" placeholder="text field" value="{{ old('gen_field_2', $record->gen_field_2) }}">

            @if ($errors->has('gen_field_2'))
                <span class="help-block">
                    <strong>{{ $errors->first('gen_field_2') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('gen_field_3') ? ' has-error' : '' }}">
            <label for="gen_field_3">Field 3</label>
            <input type="text" class="form-control" name="gen_field_3" placeholder="text field" value="{{ old('gen_field_3', $record->gen_field_3) }}">

            @if ($errors->has('gen_field_3'))
                <span class="help-block">
                    <strong>{{ $errors->first('gen_field_3') }}</strong>
                </span>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

</div>
<!-- /.col-md-5 -->

<div class="col-md-5">

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('picture_1') ? ' has-error' : '' }}">
            @if(isset($record->pictures[0]))
                <span> <img style="max-width:150px; border:1px solid gray; border-radius:2%;" src="{{ $record->pictures[0] ? env('APP_URL').$record->pictures[0]->path : '' }}" alt="image"></span>
            @endif
            {!! Form::label('picture_1', 'Picture 1', ['class' => 'control-label']) !!}
            {!! Form::file('picture_1', ['placeholder' => 'Select image']) !!}
            <p class="help-block"></p>
            @if($errors->has('picture_1'))
                <p class="help-block">
                    {{ $errors->first('picture_1') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('picture_2') ? ' has-error' : '' }}">
            @if(isset($record->pictures[1]))
                <span> <img style="max-width:150px; border:1px solid gray; border-radius:2%;" src="{{ $record->pictures[1] ? env('APP_URL').$record->pictures[1]->path : '' }}" alt="image"></span>
            @endif
            {!! Form::label('picture_2', 'Picture 2', ['class' => 'control-label']) !!}
            {!! Form::file('picture_2', ['placeholder' => 'Select image']) !!}
            <p class="help-block"></p>
            @if($errors->has('picture_2'))
                <p class="help-block">
                    {{ $errors->first('picture_2') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('picture_3') ? ' has-error' : '' }}">
            @if( isset($record->pictures[2]))
                <span> <img style="max-width:150px; border:1px solid gray; border-radius:2%;" src="{{ $record->pictures[2] ? env('APP_URL').$record->pictures[2]->path : '' }}" alt="image"></span>
            @endif
            {!! Form::label('picture_3', 'Picture 3', ['class' => 'control-label']) !!}
            {!! Form::file('picture_3', ['placeholder' => 'Select image']) !!}
            <p class="help-block"></p>
            @if($errors->has('picture_3'))
                <p class="help-block">
                    {{ $errors->first('picture_3') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('status') ? ' has-error' : '' }}">
            {!! Form::label('status', 'Status', ['class' => 'control-label']) !!}
            {!! Form::select('status', $viewResources['asset_statuses'], $record->status !== null ? $record->status : null, ['class' => 'form-control select2', 'placeholder' => 'Select Value']) !!}
            <p class="help-block"></p>
            @if($errors->has('$record->status'))
                <p class="help-block">
                    {{ $errors->first('$record->status') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('orientation') ? ' has-error' : '' }}">
            {!! Form::label('orientation', 'Orientation', ['class' => 'control-label']) !!}
            {!! Form::select('orientation', $viewResources['orientation_names'], $record->orientation !== null ? $record->orientation : null, ['class' => 'form-control select2', 'placeholder' => 'Select Value']) !!}
            <p class="help-block"></p>
            @if($errors->has('orientation'))
                <p class="help-block">
                    {{ $errors->first('orientation') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('typology') ? ' has-error' : '' }}">
            {!! Form::label('typology', 'Typology', ['class' => 'control-label']) !!}
            {!! Form::select('typology', $viewResources['typologies_names'], $record->typology !== null ? $record->typology : null, ['class' => 'form-control select2', 'placeholder' => 'Select Value']) !!}
            <p class="help-block"></p>
            @if($errors->has('typology'))
                <p class="help-block">
                    {{ $errors->first('typology') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->

    <div class="col-md-12">
        <div class="form-group margin-b-5 margin-t-5{{ $errors->has('entity') ? ' has-error' : '' }}">
            {!! Form::label('entity', 'Entity', ['class' => 'control-label']) !!}
            {!! Form::select('entity', $viewResources['entities'], ($record->asset && $record->asset->entity) ? $record->asset->entity->id : null, ['class' => 'form-control select2', 'placeholder' => 'Select Value']) !!}
            <p class="help-block"></p>
            @if($errors->has('entity'))
                <p class="help-block">
                    {{ $errors->first('entity') }}
                </p>
            @endif
        </div>
        <!-- /.form-group -->
    </div>
    <!-- /.col-md-12 -->
</div>
<!-- /.col-md-5 -->