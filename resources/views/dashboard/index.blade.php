{{-- Extends Layout --}}
@extends('layouts.backend')

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection

{{-- Page Title --}}
@section('page-title', 'Dashboard')

{{-- Page Subtitle --}}
@section('page-subtitle', 'it all starts here')

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')

    <!-- Small boxes (Stat box) -->
    <div class="row">
            @if (Auth::user()->can('view any Project content') || Auth::user()->can('view own Project content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{ $reports->getTotalProjects() }}</h3>
                        <p>Project</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-file" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::projects.index') }}" class="small-box-footer">List Projects<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
            @if (Auth::user()->can('view any Group content') || Auth::user()->can('view own Group content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-olive">
                    <div class="inner">
                        <h3>{{ $reports->getTotalGroups() }}</h3>
                        <p>Groups</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-users" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::groups.index') }}" class="small-box-footer">List Groups<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
            @if (Auth::user()->can('view any Entity content') || Auth::user()->can('view own Entity content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-orange">
                    <div class="inner">
                        <h3>{{ $reports->getTotalEntities() }}</h3>
                        <p>Entities</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-object-group" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::entities.index') }}" class="small-box-footer">List Entities<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
            @if (Auth::user()->can('view any SimpleNumber content') || Auth::user()->can('view own SimpleNumber content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalSimpleNumbers() }}</h3>
                        <p>Field > Number</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-percent" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::simple_numbers.index') }}" class="small-box-footer">List Numbers<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
            @if (Auth::user()->can('view any SimpleText content') || Auth::user()->can('view own SimpleText content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalSimpleTexts() }}</h3>
                        <p>Field > Text</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-font" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::simple_texts.index') }}" class="small-box-footer">List Texts<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
            @if (Auth::user()->can('view any WebLink content') || Auth::user()->can('view own WebLink content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalWebLinks() }}</h3>
                        <p>Field > Links</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-link" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::web_links.index') }}" class="small-box-footer">List Links<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
            @if (Auth::user()->can('view any Iframe content') || Auth::user()->can('view own Iframe content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalIframes() }}</h3>
                        <p>Field > Iframes</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-external-link" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::iframes.index') }}" class="small-box-footer">List Iframes<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
    
            @if (Auth::user()->can('view any Email content') || Auth::user()->can('view own Email content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalEmails() }}</h3>
                        <p>Field > Emails</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::emails.index') }}" class="small-box-footer">List Emails<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
    
            @if (Auth::user()->can('view any SimpleSelect content') || Auth::user()->can('view own SimpleSelect content'))
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalSelects() }}</h3>
                        <p>Field > Selects</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-list-ul" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::emails.index') }}" class="small-box-footer">List Selects<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
    
            @if (Auth::user()->can('view any SimpleImage content') || Auth::user()->can('view own SimpleImage content'))
            <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalSimpleImages() }}</h3>
                        <p>Field > Images</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-file-image-o" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::simple_images.index') }}" class="small-box-footer">List Images<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
    
            @if (Auth::user()->can('view any ApartmentAsset content') || Auth::user()->can('view own ApartmentAsset content'))
            <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>{{ $reports->getTotalApartmentAsset() }}</h3>
                        <p>Field > Apartment</p>
                    </div>
                    <div class="icon smaller">
                        <i class="fa fa-home" aria-hidden="true"></i>
                    </div>
                    <a href="{{ route('admin::apartment_assets.index') }}" class="small-box-footer">List Apartments<i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            @endif
            <!-- ./col -->
        </div>
    <!-- /.row -->

@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')

@endsection
