<!-- sidebar menu: : style can be found in sidebar.less -->
@inject('request', 'Illuminate\Http\Request')
<ul class="sidebar-menu" data-widget="tree">
    <li class="header">@lang('global.MAIN_NAVIGATION')</li>
    <li class="{{ \App\Utils::checkRoute(['dashboard::index', 'admin::index']) ? 'active': '' }}">
        <a href="{{ route('dashboard::index') }}">
            <i class="fa fa-dashboard"></i> <span>@lang('global.dashboard')</span>
        </a>
    </li>
    @can('manage users') 
    <li class="treeview {{ \App\Utils::checkRoute(['admin::users.index', 'admin::users.create', 
                                                'admin::permissions.index', 'admin::permissions.create',
                                                'admin::roles.index', 'admin::roles.create',
                                                'admin::permissions.permissions_administer']) ? 'active': ''}}">
        <a href="#">
            <i class="fa fa-users"></i>
            <span class="title">@lang('global.user-management.title')</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            @if (Auth::user()->can('view any User content') || Auth::user()->can('view own User content'))
                <li class="{{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::users.index') }}">
                        <i class="fa fa-user-secret"></i> <span>@lang('global.users')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any Permission content') || Auth::user()->can('view own Permission content'))
                <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::permissions.index') }}">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">
                            @lang('global.permissions.title')
                        </span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any Role content') || Auth::user()->can('view own Role content'))
                <li class="{{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::roles.index') }}">
                        <i class="fa fa-briefcase"></i>
                        <span class="title">
                            @lang('global.roles.title')
                        </span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any Schema content') || Auth::user()->can('view own Schema content'))
                <li class="{{ $request->segment(2) == 'schemas' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::schemas.index') }}">
                        <i class="fa fa-codepen"></i>
                        <span class="title">
                            @lang('global.schemas.title')
                        </span>
                    </a>
                </li>
            @endif

            @if( \App\Utils::checkRoute(['admin::permissions.permissions_administer']) )
                <li class="{{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}">
                        <a href="{{ route('admin::permissions.permissions_administer') }}">
                            <i class="fa fa-briefcase"></i>
                            <span class="title">
                                @lang('global.permissions.administer_permissions')
                            </span>
                        </a>
                </li>
            @endif
        </ul>
    </li>
    @endcan

    @if (Auth::user()->can('view any Company content') || Auth::user()->can('view own Company content'))
        <li class="{{ \App\Utils::checkRoute(['admin::companies.index', 'admin::companies.create']) ? 'active': '' }}">
            <a href="{{ route('admin::companies.index') }}">
                <i class="fa fa-building"></i> <span>@lang('global.companies')</span>
            </a>
        </li>
    @endif

    {{-- <li class="treeview {{ \App\Utils::checkRoute(['admin::companies.index', 'admin::companies.create',
            'admin::typologies.index', 'admin::typologies.create']) ? 'active': '' }}">

        <a href="{{ route('admin::companies.index')}}">
            <i class="fa fa-briefcase"></i>
            <span class="title">@lang('global.companies.title')</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            @if (Auth::user()->can('view any Typology content') || Auth::user()->can('view own Typology content'))
                <li class="{{ \App\Utils::checkRoute(['admin::typologies.index', 'admin::typologies.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::typologies.index') }}">
                        <i class="fa fa-building"></i> <span>@lang('global.typologies')</span>
                    </a>
                </li>
            @endif
        </ul>
    </li> --}}
    

    @if (Auth::user()->can('view any Agent content') || Auth::user()->can('view own Agent content'))
        <li class="{{ \App\Utils::checkRoute(['admin::agents.index', 'admin::agents.create']) ? 'active': '' }}">
            <a href="{{ route('admin::agents.index') }}">
                <i class="fa fa-handshake-o"></i> <span>@lang('global.agents')</span>
            </a>
        </li>
    @endif
    
    
    <li class="treeview {{ \App\Utils::checkRoute(['admin::projects.index', 'admin::projects.create',
    'admin::typologies.index', 'admin::typologies.create']) ? 'active': '' }}">

        <a href="{{ route('admin::projects.index')}}">
            <i class="fa fa-briefcase"></i>
            <span class="title">@lang('global.projects.title')</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            @if (Auth::user()->can('view any Project content') || Auth::user()->can('view own Project content'))
                <li class="{{ \App\Utils::checkRoute(['admin::projects.index', 'admin::projects.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::projects.index') }}">
                        <i class="fa fa-file"></i> <span>@lang('global.projects')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any Typology content') || Auth::user()->can('view own Typology content'))
                <li class="{{ \App\Utils::checkRoute(['admin::typologies.index', 'admin::typologies.create']) ? 'active': '' }}">
                    <a href="{{ route('admin::typologies.index') }}">
                        <i class="fa fa-building"></i> <span>@lang('global.typologies')</span>
                    </a>
                </li>
            @endif
        </ul>
    </li>

    @if (Auth::user()->can('view any Group content') || Auth::user()->can('view own Group content'))
        <li class="{{ \App\Utils::checkRoute(['admin::groups.index', 'admin::groups.create']) ? 'active': '' }}">
            <a href="{{ route('admin::groups.index') }}">
                <i class="fa fa-users"></i> <span>@lang('global.groups')</span>
            </a>
        </li>
    @endif
    @if (Auth::user()->can('view any Entity content') || Auth::user()->can('view own Entity content'))
        <li class="{{ \App\Utils::checkRoute(['admin::entities.index', 'admin::entities.create']) ? 'active': '' }}">
            <a href="{{ route('admin::entities.index') }}">
                <i class="fa fa-object-group"></i> <span>@lang('global.entities')</span>
            </a>
        </li>   
    @endif

    @can('manage assets')
    <li class="treeview {{ \App\Utils::checkRoute(['admin::simple_texts.index', 'admin::simple_texts.create',
                                                'admin::simple_numbers.index', 'admin::simple_numbers.create',
                                                'admin::web_links.index', 'admin::web_links.create',
                                                'admin::iframes.index', 'admin::iframes.create',
                                                'admin::emails.index', 'admin::emails.create',
                                                'admin::simple_images.index', 'admin::simple_images.create',
                                                'admin::apartment_assets.index', 'admin::apartment_assets.create']) ? 'active': '' }}">

        <a href="{{ route('admin::entities.index')}}">
            <i class="fa fa-briefcase"></i>
            <span class="title">@lang('global.assets.title')</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            @if (Auth::user()->can('view any SimpleText content') || Auth::user()->can('view own SimpleText content'))
                <li class="{{ $request->segment(2) == 'simple_texts' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::simple_texts.index') }}">
                        <i class="fa fa-font"></i> <span>@lang('global.simple_texts')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any SimpleNumber content') || Auth::user()->can('view own SimpleNumber content'))
                <li class="{{ $request->segment(2) == 'simple_numbers' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::simple_numbers.index') }}">
                        <i class="fa fa-percent"></i> <span>@lang('global.simple_numbers')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any WebLink content') || Auth::user()->can('view own WebLink content'))
                <li class="{{ $request->segment(2) == 'web_links' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::web_links.index') }}">
                        <i class="fa fa-link"></i> <span>@lang('global.web_links')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any admin::iframes content') || Auth::user()->can('view own Iframe content'))
                <li class="{{ $request->segment(2) == 'iframes' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::iframes.index') }}">
                        <i class="fa fa-external-link"></i> <span>@lang('global.iframes')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any Email content') || Auth::user()->can('view own Email content'))
                <li class="{{ $request->segment(2) == 'emails' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::emails.index') }}">
                        <i class="fa fa-envelope"></i> <span>@lang('global.emails')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any SimpleSelect content') || Auth::user()->can('view own SimpleSelect content'))
                <li class="{{ $request->segment(2) == 'simple_selects' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::simple_selects.index') }}">
                        <i class="fa fa-list-ul"></i> <span>@lang('global.simple_selects')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any SimpleImage content') || Auth::user()->can('view own SimpleImage content'))
                <li class="{{ $request->segment(2) == 'simple_images' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::simple_images.index') }}">
                        <i class="fa fa-file-image-o"></i> <span>@lang('global.simple_images')</span>
                    </a>
                </li>
            @endif

            @if (Auth::user()->can('view any ApartmentAsset content') || Auth::user()->can('view own ApartmentAsset content'))
                <li class="{{ $request->segment(2) == 'apartment_assets' ? 'active active-sub' : '' }}">
                    <a href="{{ route('admin::apartment_assets.index') }}">
                        <i class="fa fa-home"></i> <span>@lang('global.apartment_assets')</span>
                    </a>
                </li>
            @endif
        </ul>
    </li>
    @endcan
    
</ul>
