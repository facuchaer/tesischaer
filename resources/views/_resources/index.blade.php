{{-- Extends Layout --}}
@extends('layouts.backend')

<?php
$_pageTitle = (isset($addVarsForView['_pageTitle']) && ! empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ucwords($resourceTitle));
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && ! empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : 'List');
$_listLink = route($resourceRoutesAlias.'.index');
$_createLink = route($resourceRoutesAlias.'.create');
$_deleteSelectedLink = Route::has($resourceRoutesAlias.'.mass_destroy') ?
    route($resourceRoutesAlias.'.mass_destroy') : false;
$_cloneSelectedLink = Route::has($resourceRoutesAlias.'.mass_clone') ?
    route($resourceRoutesAlias.'.mass_clone') : false;
$canUpdate = (Auth::user()->can('edit any '.$permissionName.' content') || Auth::user()->can('edit own '.$permissionName.' content'));
$canDelete = (Auth::user()->can('delete any '.$permissionName.' content') || Auth::user()->can('delete own '.$permissionName.' content'));
$canImpersonate = false;//Auth::user()->can('impersonate', $record); TODO--- IMPLEMENT IMPERSONATE ROUTES.
//$tableCounter = 0;
$total = 0;
if (count($records) > 0) {
    $total = $records->total();
    // $tableCounter = ($records->currentPage() - 1) * $records->perPage();
    // $tableCounter = $tableCounter > 0 ? $tableCounter : 0;
}
?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render($resourceRoutesAlias) !!}
@endsection

{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')
    @parent
@endsection

@section('content')

    <!-- Default box -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">{{ $_pageSubtitle }}</h3>

            <!-- Search -->
            <div class="box-tools pull-right" style="display:inline-flex">
                <form class="form" role="form" method="GET" action="{{ $_listLink }}" class="df">
                    <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;">
                        <input type="text" name="search" class="form-control" value="{{ $search }}" placeholder="Search...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                        </div>
                    </div>
                </form>
                @if(count($records) > 0)
                    @if($_cloneSelectedLink && Auth::user()->can('create '.$permissionName.' content'))
                        <form class="form df js-form-selected" role="form" action="{{ $_cloneSelectedLink }}" method="POST" id="js-form-selected" >
                            @csrf
                            <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 100px;">
                                <input type="number" name="clone-amount" class="form-control" placeholder="@lang('global.actions.clone.amount')" required>
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary" title="clone"><i class="fa fa-clone"></i></button>
                                </div>
                            </div>
                        </form>
                    @endif
                    @if($_deleteSelectedLink && (Auth::user()->can('delete any '.$permissionName.' content') || Auth::user()->can('delete own '.$permissionName.' content')))
                        <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;">
                            <a href="{{ $_deleteSelectedLink }}" class="btn btn-sm btn-danger pull-right js-delete-selected">
                                <i class="fa fa-trash"></i> <span>Delete selected</span>
                            </a>
                        </div>
                    @endif
                @endif
                @if( Auth::user()->can('create '.$permissionName.' content'))
                    <div class="input-group input-group-sm margin-r-5 pull-left" style="width: 200px;"> 
                        <a href="{{ $_createLink }}" class="btn btn-sm btn-primary pull-right">
                            <i class="fa fa-plus"></i> <span>Add</span>
                        </a>
                    </div>
                @endif
                
            </div>
            <!-- END Search -->
        </div>

        @includeIf($resourceAlias.'._search')

        <div class="box-body no-padding">
            @if (count($records) > 0)
                <div class="padding-5">
                    <span class="text-green padding-l-5">Total: {{ $total }} items.</span>&nbsp;
                </div>
                @include($resourceAlias.'.table')
            @else
                <p class="margin-l-5 lead text-green">No records found.</p>
            @endif
        </div>
        <!-- /.box-body -->
        @if (count($records) > 0)
            @include('common.paginate', ['records' => $records])
        @endif

    </div>
    <!-- /.box -->

@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    @include('_resources._list-footer-extras', ['sortByParams' => []])
@endsection
