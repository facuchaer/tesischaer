{{-- Extends Layout --}}
@extends('layouts.backend')

<?php
$_pageTitle = (isset($addVarsForView['_pageTitle']) && !empty($addVarsForView['_pageTitle']) ? $addVarsForView['_pageTitle'] : ucwords($resourceTitle));
$_pageSubtitle = (isset($addVarsForView['_pageSubtitle']) && !empty($addVarsForView['_pageSubtitle']) ? $addVarsForView['_pageSubtitle'] : "Add " . str_singular($_pageTitle));
$_formFiles = isset($addVarsForView['formFiles']) ? $addVarsForView['formFiles'] : false;
$_listLink = route($resourceRoutesAlias.'.index');
$_createLink = route($resourceRoutesAlias.'.create');
$_storeLink = route($resourceRoutesAlias.'.store');
?>

{{-- Breadcrumbs --}}
@section('breadcrumbs')
    {!! Breadcrumbs::render($resourceRoutesAlias.'.create') !!}
@endsection

{{-- Page Title --}}
@section('page-title', $_pageTitle)

{{-- Page Subtitle --}}
@section('page-subtitle', $_pageSubtitle)

{{-- Header Extras to be Included --}}
@section('head-extras')

@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">

            <!-- Edit Form -->
            <div class="box box-info" id="wrap-edit-box">

                <form class="form" role="form" method="POST" action="{{ $_storeLink }}" {!! $_formFiles === true ? 'enctype="multipart/form-data"' : '' !!}>
                    {{ csrf_field() }}

                    {{ redirect_back_field() }}

                    <div class="box-header with-border">
                        <ul class="nav nav-pills" id="pills-tab" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link" id="pills-content-tab" data-toggle="pill" href="#pills-content" role="tab" aria-controls="pills-content" aria-selected="true">
                                    New {{ method_exists($record,'getRecordTitle') ? $record->getRecordTitle() : $record->id }}
                                </a>
                            </li>
                            @can('view admin tabs')
                                @if(View::exists($resourceAlias.'.admin-form'))
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-admin-tab" data-toggle="pill" href="#pills-admin" role="tab" aria-controls="pills-admin" aria-selected="false"> 
                                            Admin 
                                        </a>
                                    </li>
                                @endif
                            @endcan
                        </ul>

                        <div class="box-tools">
                            <a href="{{ $_listLink }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-search"></i> <span>List</span>
                            </a>
                            <a href="{{ $_createLink }}" class="btn btn-sm btn-primary margin-r-5 margin-l-5">
                                <i class="fa fa-plus"></i> <span>Add</span>
                            </a>
                            <button class="btn btn-sm btn-info margin-r-5 margin-l-5">
                                <i class="fa fa-save"></i> <span>Save</span>
                            </button>
                        </div>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body tab-content" id="pills-tabContent">
                        <div class="tab-pane fade in active" id="pills-content" role="tabpanel" aria-labelledby="pills-content-tab">
                                @include($resourceAlias.'.form')
                        </div>
                        @if(View::exists($resourceAlias.'.admin-form'))
                            <div class="tab-pane fade" id="pills-admin" role="tabpanel" aria-labelledby="pills-admin-tab">
                                    @include($resourceAlias.'.admin-form')
                            </div>
                        @endif
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer clearfix">
                        <!-- Edit Button -->
                        <div class="col-xs-4">
                            <div class="text-center margin-b-5 margin-t-5">
                                <button class="btn btn-info">
                                    <i class="fa fa-save"></i> <span>Save</span>
                                </button>
                            </div>
                        </div>
                        <!-- Clone Button -->
                        <div class="col-xs-4">
                            <div class="text-center margin-b-5 margin-t-5">
                                <button class="btn btn-info btn-clone-entity">
                                    <i class="fa fa-clone"></i> <span>Save & Clone</span>
                                </button>
                            </div>
                        </div>
                        <!-- /.col-xs-4 -->
                        <div class="col-xs-4">
                            <div class="text-center margin-b-5 margin-t-5">
                                <a href="{{ $_listLink }}" class="btn btn-default">
                                    <i class="fa fa-ban"></i> <span>Cancel</span>
                                </a>
                            </div>
                        </div>
                        <!-- /.col-xs-4 -->
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
            <!-- /.box -->
            <!-- /End Edit Form -->
        </div>
    </div>
    <!-- /.row -->
@endsection

{{-- Footer Extras to be Included --}}
@section('footer-extras')
    
@endsection
