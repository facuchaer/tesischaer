<?php
return[
    /**
     * -------------------------
     * Superbackend Version
     * --------------------------
     * 
     */

    'version' => '0.0.1',

    /**
     * -----------------------------
     * Api enabled
     * -----------------------------
     * false will only be backend without api
     * true will create routes for api.
     * 
     * default true.
     */
    'api_enabled' => env('API_ENABLED', true),
    

    /**
     * -----------------------------
     * Backend enabled
     * -----------------------------
     * 
     * false will make it headless
     * true will create routes for backend.
     * 
     * default true.
     */
    'backend_enabled' => env('BACKEND_ENABLED', true)

];