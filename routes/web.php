<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index')->name('welcome');

/**
 * Register the typical authentication routes for an application.
 * Replacing: Auth::routes();
 */
Route::group(['namespace' => 'Auth'], function () {
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login');

    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');

    // Registration Routes...
    if (config('adminlte.registration_open')) {
        Route::get('register', 'RegisterController@showRegistrationForm')->name('register');

        Route::post('register', 'RegisterController@register');
    }

    // Password Reset Routes...
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');

    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset', 'ResetPasswordController@reset');

    if (config('adminlte.impersonate')) {

        /**
         * Stop Impersonate. Requires authentication.
         */
        Route::get('impersonate/stop', 'ImpersonateController@stopImpersonate')->name('impersonate.stop');

        /**
         * Impersonate User. Requires authentication.
         */
        Route::post('impersonate/{id}', 'ImpersonateController@impersonate')->name('impersonate');
    }
});

// Redirect to /dashboard
Route::get('/home', 'HomeController@index')->name('home');

/**
 * Requires authentication.
 */
Route::group(['middleware' => 'auth'], function () {
    /**
     * Dashboard. Common access.
     * // Matches The "/dashboard/*" URLs
     */
    Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'as' => 'dashboard::'], function () {
        /**
         * Dashboard Index
         * // Route named "dashboard::index"
         */
        Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

        /**
         * Profile
         * // Route named "dashboard::profile"
         */
        Route::get('profile', ['as' => 'profile', 'uses' => 'ProfileController@showProfile']);

        Route::post('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@updateProfile']);
    });

    /**
     * // Matches The "/admin/*" URLs
     */
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'as' => 'admin::'], function () {
        /**
         * Admin Access
         */
        Route::group(['middleware' => 'admin'], function () {
            /**
             * Admin Index
             * // Route named "admin::index"
             */
            Route::get('/', ['as' => 'index', 'uses' => 'IndexController@index']);

            //Route::get('permissions_administer', ['uses' => 'PermissionsController@administer_permissions', 'as' => 'permissions.permissions_administer']);

            /**
             * Manage Users.
             * // Routes name "admin.users.*"
             */
            Route::resource('users', 'UsersController');
            Route::resource('roles', 'RolesController');
            Route::resource('emails', 'EmailsController');
            Route::resource('groups', 'GroupsController');
            Route::resource('agents', 'AgentsController');
            Route::resource('schemas', 'SchemasController');
            Route::resource('iframes', 'IframesController');
            Route::resource('entities', 'EntitiesController');
            Route::resource('projects', 'ProjectsController');
            Route::resource('web_links', 'WebLinksController');
            Route::resource('companies', 'CompaniesController');
            Route::resource('typologies', 'TypologiesController');
            Route::resource('permissions', 'PermissionsController');
            Route::resource('simple_texts', 'SimpleTextsController');
            Route::resource('simple_images', 'SimpleImagesController');
            Route::resource('simple_numbers', 'SimpleNumbersController');
            Route::resource('simple_selects', 'SimpleSelectsController');
            Route::resource('apartment_assets', 'ApartmentAssetController');
            
            //MASS DELETE
            Route::post('users_mass_destroy', ['uses' => 'UsersController@massDestroy', 'as' => 'users.mass_destroy']);
            Route::post('roles_mass_destroy', ['uses' => 'RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
            Route::post('emails_mass_destroy', ['uses' => 'EmailsController@massDestroy', 'as' => 'emails.mass_destroy']);
            Route::post('groups_mass_destroy', ['uses' => 'GroupsController@massDestroy', 'as' => 'groups.mass_destroy']);
            Route::post('agents_mass_destroy', ['uses' => 'AgentsController@massDestroy', 'as' => 'agents.mass_destroy']);
            Route::post('iframes_mass_destroy', ['uses' => 'IframesController@massDestroy', 'as' => 'iframes.mass_destroy']);
            Route::post('schemas_mass_destroy', ['uses' => 'SchemasController@massDestroy', 'as' => 'schemas.mass_destroy']);
            Route::post('entities_mass_destroy', ['uses' => 'EntitiesController@massDestroy', 'as' => 'entities.mass_destroy']);
            Route::post('projects_mass_destroy', ['uses' => 'ProjectsController@massDestroy', 'as' => 'projects.mass_destroy']);
            Route::post('web_links_mass_destroy', ['uses' => 'WebLinksController@massDestroy', 'as' => 'web_links.mass_destroy']);
            Route::post('companies_mass_destroy', ['uses' => 'CompaniesController@massDestroy', 'as' => 'companies.mass_destroy']);
            Route::post('typologies_mass_destroy', ['uses' => 'TypologiesController@massDestroy', 'as' => 'typologies.mass_destroy']);
            Route::post('permissions_mass_destroy', ['uses' => 'PermissionsController@massDestroy', 'as' => 'permissions.mass_destroy']);
            Route::post('simple_texts_mass_destroy', ['uses' => 'SimpleTextsController@massDestroy', 'as' => 'simple_texts.mass_destroy']);
            Route::post('simple_images_mass_destroy', ['uses' => 'SimpleImagesController@massDestroy', 'as' => 'simple_images.mass_destroy']);
            Route::post('simple_numbers_mass_destroy', ['uses' => 'SimpleNumbersController@massDestroy', 'as' => 'simple_numbers.mass_destroy']);
            Route::post('simple_selects_mass_destroy', ['uses' => 'SimpleSelectsController@massDestroy', 'as' => 'simple_selects.mass_destroy']);
            Route::post('apartment_assets_mass_destroy', ['uses' => 'ApartmentAssetController@massDestroy', 'as' => 'apartment_assets.mass_destroy']);

            //MASS CLONE
            Route::post('users_mass_clone', ['uses' => 'UsersController@massClone', 'as' => 'users.mass_clone']);
            Route::post('roles_mass_clone', ['uses' => 'RolesController@massClone', 'as' => 'roles.mass_clone']);
            Route::post('emails_mass_clone', ['uses' => 'EmailsController@massClone', 'as' => 'emails.mass_clone']);
            Route::post('groups_mass_clone', ['uses' => 'GroupsController@massClone', 'as' => 'groups.mass_clone']);
            Route::post('agents_mass_clone', ['uses' => 'AgentsController@massClone', 'as' => 'agents.mass_clone']);
            Route::post('schemas_mass_clone', ['uses' => 'SchemasController@massClone', 'as' => 'schemas_mass_clone']);
            Route::post('iframes_mass_clone', ['uses' => 'IframesController@massClone', 'as' => 'iframes.mass_clone']);
            Route::post('entities_mass_clone', ['uses' => 'EntitiesController@massClone', 'as' => 'entities.mass_clone']);
            Route::post('projects_mass_clone', ['uses' => 'ProjectsController@massClone', 'as' => 'projects.mass_clone']);
            Route::post('web_links_mass_clone', ['uses' => 'WebLinksController@massClone', 'as' => 'web_links.mass_clone']);
            Route::post('companies_mass_clone', ['uses' => 'CompaniesController@massClone', 'as' => 'companies.mass_clone']);
            Route::post('typologies_mass_clone', ['uses' => 'TypologiesController@massClone', 'as' => 'typologies.mass_clone']);
            Route::post('permissions_mass_clone', ['uses' => 'PermissionsController@massClone', 'as' => 'permissions.mass_clone']);
            Route::post('simple_texts_mass_clone', ['uses' => 'SimpleTextsController@massClone', 'as' => 'simple_texts.mass_clone']);
            Route::post('simple_images_mass_clone', ['uses' => 'SimpleImagesController@massClone', 'as' => 'simple_images.mass_clone']);
            Route::post('simple_numbers_mass_clone', ['uses' => 'SimpleNumbersController@massClone', 'as' => 'simple_numbers.mass_clone']);
            Route::post('simple_selects_mass_clone', ['uses' => 'SimpleSelectsController@massClone', 'as' => 'simple_selects.mass_clone']);
            Route::post('apartment_assets_mass_clone', ['uses' => 'ApartmentAssetController@massClone', 'as' => 'apartment_assets.mass_clone']);
        });
    });

});
