<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

if (config('superbackend.api_enabled')) {

    Route::group(['middleware' => 'api', 'prefix' => 'auth', 'namespace' => 'Auth'], function () {
        Route::post('login', 'AuthController@login');
        // Registration Routes...
        if (config('adminlte.registration_open')) {
            Route::post('signup', 'AuthController@signup');
        }
        Route::get('signup/activate/{token}', 'AuthController@signupActivate');

        Route::group(['middleware' => 'auth:api'], function () {
            Route::get('logout', 'AuthController@logout');
            Route::get('user', 'AuthController@user');
        });

        // Route::get('/company', function () {
        //     return new CompanyResource(Company::find(1));
        // });
    });

    Route::group(['middleware' => 'api', 'prefix' => 'password', 'namespace' => 'Auth'], function () {
        Route::post('create', 'PasswordResetController@create');
        Route::get('find/{token}', 'PasswordResetController@find');
        Route::post('reset', 'PasswordResetController@reset');
    });

    Route::group(['middleware' => 'auth:api', 'prefix' => 'v1', 'namespace' => 'Api'], function () {
        Route::apiResources([
            'agent' => 'AgentsController',
            'email' => 'EmailsController',
            'group' => 'GroupsController',
            'asset' => 'AssetsController',
            'iframe' => 'IframesController',
            'entity' => 'EntitiesController',
            'project' => 'ProjectsController',
            'web_link' => 'WebLinksController',
            'company' => 'CompaniesController',
            'simple_text' => 'SimpleTextsController',
            'simple_image' => 'SimpleImagesController',
            'simple_number' => 'SimpleNumbersController',
            'apartment_asset' => 'ApartmentAssetsController'
        ]);
    });
}
