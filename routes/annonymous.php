<?php

Route::group(['middleware' => 'annonymous', 'prefix' => 'v1', 'namespace' => 'Api'], function () {
    Route::get('appartment_asset/{company}', 'ApartmentAssetsController@annonymousIndex');
});